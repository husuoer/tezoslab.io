����      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Michelson Anti-Patterns�h]�h �Text����Michelson Anti-Patterns�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�A/home/greg/Tezos/tezos/docs/tutorials/michelson_anti_patterns.rst�hKubh �	paragraph���)��}�(hXi  Even though Michelson is designed to make it easy to write secure
contracts and difficult to write vulnerable ones, it is still possible
to write buggy contracts that leak data and funds. This is a list of
mistakes that you can make when writing or interacting with contracts on
the Tezos blockchain and alternative ways to write code that avoid these
problems.�h]�hXi  Even though Michelson is designed to make it easy to write secure
contracts and difficult to write vulnerable ones, it is still possible
to write buggy contracts that leak data and funds. This is a list of
mistakes that you can make when writing or interacting with contracts on
the Tezos blockchain and alternative ways to write code that avoid these
problems.�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh,)��}�(h��Note: We are currently reworking the concurrency model of Michelson (how
and when sub-transactions are made), so that some of these patterns will
be prevented by the language itself.�h]�h��Note: We are currently reworking the concurrency model of Michelson (how
and when sub-transactions are made), so that some of these patterns will
be prevented by the language itself.�����}�(hh=hh;hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh
)��}�(hhh]�(h)��}�(h� Refunding to a list of contracts�h]�h� Refunding to a list of contracts�����}�(hhNhhLhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhhIhhhh*hKubh,)��}�(h��One common pattern in contracts is to refund a group of people’s funds
at once. This is problematic if you accepted arbitrary contracts as a
malicious user can do cause various issues for you.�h]�h��One common pattern in contracts is to refund a group of people’s funds
at once. This is problematic if you accepted arbitrary contracts as a
malicious user can do cause various issues for you.�����}�(hh\hhZhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhIhhubh
)��}�(hhh]�(h)��}�(h�Possible issues:�h]�h�Possible issues:�����}�(hhmhhkhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhhhhhhh*hKubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�?One contract swallows all the gas through a series of callbacks�h]�h,)��}�(hh�h]�h�?One contract swallows all the gas through a series of callbacks�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hh{hhhh*hNubh)��}�(h�8One contract writes transactions until the block is full�h]�h,)��}�(hh�h]�h�8One contract writes transactions until the block is full�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hh{hhhh*hNubh)��}�(h�mReentrancy bugs. Michelson intentionally makes these difficult to
write, but it is still possible if you try.�h]�h,)��}�(h�mReentrancy bugs. Michelson intentionally makes these difficult to
write, but it is still possible if you try.�h]�h�mReentrancy bugs. Michelson intentionally makes these difficult to
write, but it is still possible if you try.�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hh{hhhh*hNubh)��}�(h�EA contract calls the \`FAIL\` instruction, stopping all computation.
�h]�h,)��}�(h�DA contract calls the \`FAIL\` instruction, stopping all computation.�h]�h�BA contract calls the `FAIL` instruction, stopping all computation.�����}�(h�DA contract calls the \`FAIL\` instruction, stopping all computation.�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hh{hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��bullet��-�uh)hyhh*hKhhhhhubeh}�(h]��possible-issues�ah!]�h#]�h%]��possible issues:�ah']�uh)h	hhIhhhh*hK�
referenced�Kubh
)��}�(hhh]�(h)��}�(h�Alternatives/Solutions:�h]�h�Alternatives/Solutions:�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hK ubhz)��}�(hhh]�(h)��}�(h��Create a default account from people’s keys. Default accounts cannot
execute code, avoiding the bugs above. Have people submit keys rather
than contracts.�h]�h,)��}�(h��Create a default account from people’s keys. Default accounts cannot
execute code, avoiding the bugs above. Have people submit keys rather
than contracts.�h]�h��Create a default account from people’s keys. Default accounts cannot
execute code, avoiding the bugs above. Have people submit keys rather
than contracts.�����}�(hj
  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK"hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj  hhhh*hNubh)��}�(h��Have people pull their funds individually. Each user can break their
own withdrawal only. **This does not protect against reentrancy
bugs.**
�h]�h,)��}�(h��Have people pull their funds individually. Each user can break their
own withdrawal only. **This does not protect against reentrancy
bugs.**�h]�(h�ZHave people pull their funds individually. Each user can break their
own withdrawal only. �����}�(h�ZHave people pull their funds individually. Each user can break their
own withdrawal only. �hj   ubh �strong���)��}�(h�2**This does not protect against reentrancy
bugs.**�h]�h�.This does not protect against reentrancy
bugs.�����}�(hhhj+  ubah}�(h]�h!]�h#]�h%]�h']�uh)j)  hj   ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK%hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK"hh�hhubeh}�(h]��alternatives-solutions�ah!]�h#]�h%]��alternatives/solutions:�ah']�uh)h	hhIhhhh*hK h�Kubeh}�(h]�� refunding-to-a-list-of-contracts�ah!]�h#]�� refunding to a list of contracts�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�DAvoid batch operations when users can increase the size of the batch�h]�h�DAvoid batch operations when users can increase the size of the batch�����}�(hj`  hj^  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj[  hhhh*hK*ubh,)��}�(h��Contracts that rely on linear or super-linear operations are vulnerable
to malicious users supplying values until the contract cannot finish
without running into fuel limits. This can deadlock your contract.�h]�h��Contracts that rely on linear or super-linear operations are vulnerable
to malicious users supplying values until the contract cannot finish
without running into fuel limits. This can deadlock your contract.�����}�(hjn  hjl  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK,hj[  hhubh �target���)��}�(h�.. _possible-issues-1:�h]�h}�(h]�h!]�h#]�h%]�h']��refid��possible-issues-1�uh)jz  hK0hj[  hhhh*ubh
)��}�(hhh]�(h)��}�(h�Possible issues:�h]�h�Possible issues:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK3ubhz)��}�(hhh]�(h)��}�(hX  Malicious users can force your contract into a pathological worst
case, stopping it from finishing with available gas. Note that in the
absence of hard gas limits, this can still be disabling as node
operators may not want to run contracts that take more than a certain
amount of gas.�h]�h,)��}�(hX  Malicious users can force your contract into a pathological worst
case, stopping it from finishing with available gas. Note that in the
absence of hard gas limits, this can still be disabling as node
operators may not want to run contracts that take more than a certain
amount of gas.�h]�hX  Malicious users can force your contract into a pathological worst
case, stopping it from finishing with available gas. Note that in the
absence of hard gas limits, this can still be disabling as node
operators may not want to run contracts that take more than a certain
amount of gas.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK5hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubh)��}�(h��You may hit the slow case of an amortized algorithm or data structure
at an inopportune time, using up all of your contract’s available
gas.
�h]�h,)��}�(h��You may hit the slow case of an amortized algorithm or data structure
at an inopportune time, using up all of your contract’s available
gas.�h]�h��You may hit the slow case of an amortized algorithm or data structure
at an inopportune time, using up all of your contract’s available
gas.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK:hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK5hj�  hhubj{  )��}�(h�.. _alternativessolutions-1:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �alternativessolutions-1�uh)jz  hK>hj�  hhhh*ubeh}�(h]�(j�  �id1�eh!]�h#]��possible-issues-1�ah%]�h�ah']�uh)h	hj[  hhhh*hK3h�K�expect_referenced_by_name�}�j�  j|  s�expect_referenced_by_id�}�j�  j|  subh
)��}�(hhh]�(h)��}�(h�Alternatives/Solutions:�h]�h�Alternatives/Solutions:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKAubhz)��}�(hhh]�(h)��}�(h�kAvoid data structures and algorithms that rely on amortized
operations, especially when users may add data.�h]�h,)��}�(h�kAvoid data structures and algorithms that rely on amortized
operations, especially when users may add data.�h]�h�kAvoid data structures and algorithms that rely on amortized
operations, especially when users may add data.�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKChj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubh)��}�(h�iRestrict the amount of data your contract can store to a level that
will not overwhelm the available gas.�h]�h,)��}�(h�iRestrict the amount of data your contract can store to a level that
will not overwhelm the available gas.�h]�h�iRestrict the amount of data your contract can store to a level that
will not overwhelm the available gas.�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKEhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubh)��}�(h��Write your contract so that it may pause and resume batch operations.
This would complicate these sequences and require constant checking
of available gas, but it prevents various attacks.
�h]�h,)��}�(h��Write your contract so that it may pause and resume batch operations.
This would complicate these sequences and require constant checking
of available gas, but it prevents various attacks.�h]�h��Write your contract so that it may pause and resume batch operations.
This would complicate these sequences and require constant checking
of available gas, but it prevents various attacks.�����}�(hj3  hj1  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKGhj-  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hKChj�  hhubh,)��}�(hXD  \*Do not assume an attack will be prohibitively expensive\*
Cryptocurrencies have extreme price fluctuations frequently and an
extremely motivated attacker may decide that an enormous expense is
justified. Remember, an attack that disables a contract is not just
targeted at the authors, but also the users of that contract.�h]�hXB  *Do not assume an attack will be prohibitively expensive*
Cryptocurrencies have extreme price fluctuations frequently and an
extremely motivated attacker may decide that an enormous expense is
justified. Remember, an attack that disables a contract is not just
targeted at the authors, but also the users of that contract.�����}�(hXD  \*Do not assume an attack will be prohibitively expensive\*
Cryptocurrencies have extreme price fluctuations frequently and an
extremely motivated attacker may decide that an enormous expense is
justified. Remember, an attack that disables a contract is not just
targeted at the authors, but also the users of that contract.�hjK  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKKhj�  hhubeh}�(h]�(j�  �id2�eh!]�h#]��alternativessolutions-1�ah%]�jQ  ah']�uh)h	hj[  hhhh*hKAh�Kj�  }�j_  j�  sj�  }�j�  j�  subeh}�(h]��Davoid-batch-operations-when-users-can-increase-the-size-of-the-batch�ah!]�h#]��Davoid batch operations when users can increase the size of the batch�ah%]�h']�uh)h	hhhhhh*hK*ubh
)��}�(hhh]�(h)��}�(h�.Signatures alone do not prevent replay attacks�h]�h�.Signatures alone do not prevent replay attacks�����}�(hjq  hjo  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjl  hhhh*hKRubh,)��}�(hXL  If your contract uses signatures to authenticate messages, beware of
replay attacks. If a user ever signs a piece of data, you *must* make
sure that that piece of data is never again a valid message to the
contract. If you do not do this, anyone else can call your contract with
the same input and piggyback on the earlier approval.�h]�(h�If your contract uses signatures to authenticate messages, beware of
replay attacks. If a user ever signs a piece of data, you �����}�(h�If your contract uses signatures to authenticate messages, beware of
replay attacks. If a user ever signs a piece of data, you �hj}  hhhNhNubh �emphasis���)��}�(h�*must*�h]�h�must�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj}  ubh�� make
sure that that piece of data is never again a valid message to the
contract. If you do not do this, anyone else can call your contract with
the same input and piggyback on the earlier approval.�����}�(h�� make
sure that that piece of data is never again a valid message to the
contract. If you do not do this, anyone else can call your contract with
the same input and piggyback on the earlier approval.�hj}  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKThjl  hhubj{  )��}�(h�.. _possible-issues-2:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �possible-issues-2�uh)jz  hKZhjl  hhhh*ubh
)��}�(hhh]�(h)��}�(h�Possible issues:�h]�h�Possible issues:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK]ubhz)��}�(hhh]�h)��}�(h�.A previously approved action can be replayed.
�h]�h,)��}�(h�-A previously approved action can be replayed.�h]�h�-A previously approved action can be replayed.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK_hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK_hj�  hhubj{  )��}�(h�.. _alternativessolutions-2:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �alternativessolutions-2�uh)jz  hKahj�  hhhh*ubeh}�(h]�(j�  �id3�eh!]�h#]��possible-issues-2�ah%]��possible issues:�ah']�uh)h	hjl  hhhh*hK]h�Kj�  }�j�  j�  sj�  }�j�  j�  subh
)��}�(hhh]�(h)��}�(h�Alternatives/Solutions�h]�h�Alternatives/Solutions�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKdubhz)��}�(hhh]�(h)��}�(h��Use an internal counter to make the data you ask users to sign
unique. This counter should be per key so that users can find out
what they need to approve. This should be paired with a signed hash
of your contract to prevent cross-contract replays.�h]�h,)��}�(h��Use an internal counter to make the data you ask users to sign
unique. This counter should be per key so that users can find out
what they need to approve. This should be paired with a signed hash
of your contract to prevent cross-contract replays.�h]�h��Use an internal counter to make the data you ask users to sign
unique. This counter should be per key so that users can find out
what they need to approve. This should be paired with a signed hash
of your contract to prevent cross-contract replays.�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKfhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj  hhhh*hNubh)��}�(h�`Use the ``SENDER`` instruction to verify that the expected sender is
the source of the message.
�h]�h,)��}�(h�_Use the ``SENDER`` instruction to verify that the expected sender is
the source of the message.�h]�(h�Use the �����}�(h�Use the �hj$  ubh �literal���)��}�(h�
``SENDER``�h]�h�SENDER�����}�(hhhj/  ubah}�(h]�h!]�h#]�h%]�h']�uh)j-  hj$  ubh�M instruction to verify that the expected sender is
the source of the message.�����}�(h�M instruction to verify that the expected sender is
the source of the message.�hj$  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKjhj   ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hKfhj�  hhubeh}�(h]�(j�  �id4�eh!]�h#]��alternativessolutions-2�ah%]��alternatives/solutions�ah']�uh)h	hjl  hhhh*hKdh�Kj�  }�jY  j�  sj�  }�j�  j�  subh
)��}�(hhh]�(h)��}�(h�BDo not assume users will use a unique key for every smart contract�h]�h�BDo not assume users will use a unique key for every smart contract�����}�(hjd  hjb  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj_  hhhh*hKnubh,)��}�(hXW  Users should always use a different key for every contract with which
they interact. If this is not the case, a message the user signed for
another contract can be sent to your contract. An internal counter alone
does not protect against this attack. It *must* be paired with a hash of
your contract. You must verify the source of the message.�h]�(h��Users should always use a different key for every contract with which
they interact. If this is not the case, a message the user signed for
another contract can be sent to your contract. An internal counter alone
does not protect against this attack. It �����}�(h��Users should always use a different key for every contract with which
they interact. If this is not the case, a message the user signed for
another contract can be sent to your contract. An internal counter alone
does not protect against this attack. It �hjp  hhhNhNubj�  )��}�(h�*must*�h]�h�must�����}�(hhhjy  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjp  ubh�S be paired with a hash of
your contract. You must verify the source of the message.�����}�(h�S be paired with a hash of
your contract. You must verify the source of the message.�hjp  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKphj_  hhubeh}�(h]��Bdo-not-assume-users-will-use-a-unique-key-for-every-smart-contract�ah!]�h#]��Bdo not assume users will use a unique key for every smart contract�ah%]�h']�uh)h	hjl  hhhh*hKnubeh}�(h]��.signatures-alone-do-not-prevent-replay-attacks�ah!]�h#]��.signatures alone do not prevent replay attacks�ah%]�h']�uh)h	hhhhhh*hKRubh
)��}�(hhh]�(h)��}�(h�!Storing/transferring private data�h]�h�!Storing/transferring private data�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKwubh,)��}�(hX�  Once data is published to anyone, including broadcasting a transaction,
that data is public. Never transmit secret information via any part of
the blockchain ecosystem. As soon as you have broadcast a transaction
including that piece of information, anyone can see it. Furthermore,
malicious nodes in the system can manipulate unsigned transactions by
delaying, modifying, or reordering them.�h]�hX�  Once data is published to anyone, including broadcasting a transaction,
that data is public. Never transmit secret information via any part of
the blockchain ecosystem. As soon as you have broadcast a transaction
including that piece of information, anyone can see it. Furthermore,
malicious nodes in the system can manipulate unsigned transactions by
delaying, modifying, or reordering them.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKyhj�  hhubj{  )��}�(h�.. _possible-issues-3:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �possible-issues-3�uh)jz  hK�hj�  hhhh*ubh
)��}�(hhh]�(h)��}�(h�Possible Issues�h]�h�Possible Issues�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK�ubhz)��}�(hhh]�(h)��}�(h�)If data is not signed, it can be modified�h]�h,)��}�(hj�  h]�h�)If data is not signed, it can be modified�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubh)��}�(h�Transactions can be delayed�h]�h,)��}�(hj�  h]�h�Transactions can be delayed�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubh)��}�(h�&Secret information will become public
�h]�h,)��}�(h�%Secret information will become public�h]�h�%Secret information will become public�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK�hj�  hhubj{  )��}�(h�.. _alternativessolutions-3:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �alternativessolutions-3�uh)jz  hK�hj�  hhhh*ubeh}�(h]�(j�  �id5�eh!]�h#]��possible-issues-3�ah%]��possible issues�ah']�uh)h	hj�  hhhh*hK�h�Kj�  }�j<  j�  sj�  }�j�  j�  subh
)��}�(hhh]�(h)��}�(h�Alternatives/Solutions�h]�h�Alternatives/Solutions�����}�(hjG  hjE  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjB  hhhh*hK�ubhz)��}�(hhh]�(h)��}�(h�SDo not store private information on the blockchain or broadcast it in
transactions.�h]�h,)��}�(h�SDo not store private information on the blockchain or broadcast it in
transactions.�h]�h�SDo not store private information on the blockchain or broadcast it in
transactions.�����}�(hj\  hjZ  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjV  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hjS  hhhh*hNubh)��}�(h�USign all transactions that contain information that, if manipulated,
could be abused.�h]�h,)��}�(h�USign all transactions that contain information that, if manipulated,
could be abused.�h]�h�USign all transactions that contain information that, if manipulated,
could be abused.�����}�(hjt  hjr  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjn  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hjS  hhhh*hNubh)��}�(h�,Use counters to enforce transaction orders.
�h]�h,)��}�(h�+Use counters to enforce transaction orders.�h]�h�+Use counters to enforce transaction orders.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hjS  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK�hjB  hhubh,)��}�(h�LThis will at least create a logical clock on messages sent to your
contract.�h]�h�LThis will at least create a logical clock on messages sent to your
contract.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjB  hhubeh}�(h]�(j6  �id6�eh!]�h#]��alternativessolutions-3�ah%]�j[  ah']�uh)h	hj�  hhhh*hK�h�Kj�  }�j�  j,  sj�  }�j6  j,  subeh}�(h]��!storing-transferring-private-data�ah!]�h#]��!storing/transferring private data�ah%]�h']�uh)h	hhhhhh*hKwubh
)��}�(hhh]�(h)��}�(h�'Not setting all state before a transfer�h]�h�'Not setting all state before a transfer�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK�ubh,)��}�(hXb  Reentrancy is a potential issue on the blockchain. When a contract makes
a transfer to another contract, that contract can execute its own code,
and can make arbitrary further transfers, including back to the original
contract. If state has not been updated before the transfer is made, a
contract can call back in and execute actions based on old state.�h]�hXb  Reentrancy is a potential issue on the blockchain. When a contract makes
a transfer to another contract, that contract can execute its own code,
and can make arbitrary further transfers, including back to the original
contract. If state has not been updated before the transfer is made, a
contract can call back in and execute actions based on old state.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubj{  )��}�(h�.. _possible-issues-4:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �possible-issues-4�uh)jz  hK�hj�  hhhh*ubh
)��}�(hhh]�(h)��}�(h�Possible Issues�h]�h�Possible Issues�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK�ubhz)��}�(hhh]�(h)��}�(h�Multiple withdrawals/actions�h]�h,)��}�(hj  h]�h�Multiple withdrawals/actions�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubh)��}�(h�9Generating illegal state if state is updated twice later
�h]�h,)��}�(h�8Generating illegal state if state is updated twice later�h]�h�8Generating illegal state if state is updated twice later�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK�hj�  hhubj{  )��}�(h�.. _alternativessolutions-4:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �alternativessolutions-4�uh)jz  hK�hj�  hhhh*ubeh}�(h]�(j�  �id7�eh!]�h#]��possible-issues-4�ah%]�j>  ah']�uh)h	hj�  hhhh*hK�h�Kj�  }�jG  j�  sj�  }�j�  j�  subh
)��}�(hhh]�(h)��}�(h�Alternatives/Solutions�h]�h�Alternatives/Solutions�����}�(hjQ  hjO  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjL  hhhh*hK�ubhz)��}�(hhh]�(h)��}�(h��Forbid reentrancy by means of a flag in your storage, unless you have
a good reason to allow users to reenter your contract, this is likely
the best option.�h]�h,)��}�(h��Forbid reentrancy by means of a flag in your storage, unless you have
a good reason to allow users to reenter your contract, this is likely
the best option.�h]�h��Forbid reentrancy by means of a flag in your storage, unless you have
a good reason to allow users to reenter your contract, this is likely
the best option.�����}�(hjf  hjd  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj`  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj]  hhhh*hNubh)��}�(hX  Only make transfers to trusted contracts or default accounts. Default
accounts cannot execute code, so it is always safe to transfer to
them. Before trusting a contract, make sure that its behavior cannot
be modified and that you have an extremely high degree of confidence
in it.
�h]�h,)��}�(hX  Only make transfers to trusted contracts or default accounts. Default
accounts cannot execute code, so it is always safe to transfer to
them. Before trusting a contract, make sure that its behavior cannot
be modified and that you have an extremely high degree of confidence
in it.�h]�hX  Only make transfers to trusted contracts or default accounts. Default
accounts cannot execute code, so it is always safe to transfer to
them. Before trusting a contract, make sure that its behavior cannot
be modified and that you have an extremely high degree of confidence
in it.�����}�(hj~  hj|  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjx  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj]  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK�hjL  hhubeh}�(h]�(jA  �id8�eh!]�h#]��alternativessolutions-4�ah%]��alternatives/solutions�ah']�uh)h	hj�  hhhh*hK�h�Kj�  }�j�  j7  sj�  }�jA  j7  subeh}�(h]��'not-setting-all-state-before-a-transfer�ah!]�h#]��'not setting all state before a transfer�ah%]�h']�uh)h	hhhhhh*hK�ubh
)��}�(hhh]�(h)��}�(h�4Do not store funds for others in spendable contracts�h]�h�4Do not store funds for others in spendable contracts�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK�ubh,)��}�(h��Tezos allows contracts to be marked as spendable. Managers of spendable
contracts can make transfers using the funds stored inside the contract.
This can subvert guarantees about the contract’s behavior that are
visible in the code.�h]�h��Tezos allows contracts to be marked as spendable. Managers of spendable
contracts can make transfers using the funds stored inside the contract.
This can subvert guarantees about the contract’s behavior that are
visible in the code.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubj{  )��}�(h�.. _possible-issues-5:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �possible-issues-5�uh)jz  hK�hj�  hhhh*ubh
)��}�(hhh]�(h)��}�(h�Possible Issues�h]�h�Possible Issues�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK�ubhz)��}�(hhh]�(h)��}�(h�'The funds of a contract can be removed.�h]�h,)��}�(hj�  h]�h�'The funds of a contract can be removed.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubh)��}�(h�3A contract may not be able to meet its obligations
�h]�h,)��}�(h�2A contract may not be able to meet its obligations�h]�h�2A contract may not be able to meet its obligations�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK�hj�  hhubj{  )��}�(h�.. _alternativessolutions-5:�h]�h}�(h]�h!]�h#]�h%]�h']�j�  �alternativessolutions-5�uh)jz  hK�hj�  hhhh*ubeh}�(h]�(j�  �id9�eh!]�h#]��possible-issues-5�ah%]��possible issues�ah']�uh)h	hj�  hhhh*hK�h�Kj�  }�j,  j�  sj�  }�j�  j�  subh
)��}�(hhh]�(h)��}�(h�Alternatives/Solutions�h]�h�Alternatives/Solutions�����}�(hj7  hj5  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj2  hhhh*hK�ubhz)��}�(hhh]�h)��}�(h�BDo not store funds in spendable contracts that you do not control.�h]�h,)��}�(hjH  h]�h�BDo not store funds in spendable contracts that you do not control.�����}�(hjH  hjJ  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjF  ubah}�(h]�h!]�h#]�h%]�h']�uh)h~hjC  hhhh*hNubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)hyhh*hK�hj2  hhubeh}�(h]�(j&  �id10�eh!]�h#]��alternativessolutions-5�ah%]��alternatives/solutions�ah']�uh)h	hj�  hhhh*hK�h�Kj�  }�jh  j  sj�  }�j&  j  subeh}�(h]��4do-not-store-funds-for-others-in-spendable-contracts�ah!]�h#]��4do not store funds for others in spendable contracts�ah%]�h']�uh)h	hhhhhh*hK�ubeh}�(h]��michelson-anti-patterns�ah!]�h#]��michelson anti-patterns�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�(j�  ]�j|  aj�  ]�j�  aj�  ]�j�  aj�  ]�j�  aj�  ]�j�  aj6  ]�j,  aj�  ]�j�  ajA  ]�j7  aj�  ]�j�  aj&  ]�j  au�nameids�}�(j{  jx  jX  jU  �possible issues:�N�alternatives/solutions:�Nji  jf  j�  j�  j_  j�  j�  j�  j�  j�  jY  j�  �alternatives/solutions�Nj�  j�  j�  j�  j<  j�  �possible issues�Nj�  j6  j�  j�  jG  j�  j�  jA  js  jp  j,  j�  jh  j&  u�	nametypes�}�(j{  NjX  Nj�  Nj�  Nji  Nj�  �j_  �j�  Nj�  �jY  �j�  Nj�  Nj�  Nj<  �j�  Nj�  �j�  NjG  �j�  �js  Nj,  �jh  �uh}�(jx  hjU  hIh�hhjM  h�jf  j[  j�  j�  j�  j�  j�  j�  j\  j�  j�  jl  j�  j�  j�  j�  j�  j�  jV  j�  j�  j_  j�  j�  j�  j�  j9  j�  j6  jB  j�  jB  j�  j�  j�  j�  jD  j�  jA  jL  j�  jL  jp  j�  j�  j�  j)  j�  j&  j2  je  j2  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�h,)��}�(h�3Duplicate implicit target name: "possible issues:".�h]�h�7Duplicate implicit target name: “possible issues:”.�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj	  ubah}�(h]�h!]�h#]�h%]�h']�j�  a�level�K�type��INFO��source�h*�line�K3uh)j  hj�  hhhh*hK3ubj  )��}�(hhh]�h,)��}�(h�:Duplicate implicit target name: "alternatives/solutions:".�h]�h�>Duplicate implicit target name: “alternatives/solutions:”.�����}�(hhhj(  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj%  ubah}�(h]�h!]�h#]�h%]�h']�j\  a�level�K�type�j"  �source�h*�line�KAuh)j  hj�  hhhh*hKAubj  )��}�(hhh]�h,)��}�(h�3Duplicate implicit target name: "possible issues:".�h]�h�7Duplicate implicit target name: “possible issues:”.�����}�(hhhjC  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj@  ubah}�(h]�h!]�h#]�h%]�h']�j�  a�level�K�type�j"  �source�h*�line�K]uh)j  hj�  hhhh*hK]ubj  )��}�(hhh]�h,)��}�(h�9Duplicate implicit target name: "alternatives/solutions".�h]�h�=Duplicate implicit target name: “alternatives/solutions”.�����}�(hhhj^  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj[  ubah}�(h]�h!]�h#]�h%]�h']�j�  a�level�K�type�j"  �source�h*�line�K�uh)j  hjB  hhhh*hK�ubj  )��}�(hhh]�h,)��}�(h�2Duplicate implicit target name: "possible issues".�h]�h�6Duplicate implicit target name: “possible issues”.�����}�(hhhjy  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hjv  ubah}�(h]�h!]�h#]�h%]�h']�jD  a�level�K�type�j"  �source�h*�line�K�uh)j  hj�  hhhh*hK�ubj  )��}�(hhh]�h,)��}�(h�9Duplicate implicit target name: "alternatives/solutions".�h]�h�=Duplicate implicit target name: “alternatives/solutions”.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']�j�  a�level�K�type�j"  �source�h*�line�K�uh)j  hjL  hhhh*hK�ubj  )��}�(hhh]�h,)��}�(h�2Duplicate implicit target name: "possible issues".�h]�h�6Duplicate implicit target name: “possible issues”.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']�j)  a�level�K�type�j"  �source�h*�line�K�uh)j  hj�  hhhh*hK�ubj  )��}�(hhh]�h,)��}�(h�9Duplicate implicit target name: "alternatives/solutions".�h]�h�=Duplicate implicit target name: “alternatives/solutions”.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']�je  a�level�K�type�j"  �source�h*�line�K�uh)j  hj2  hhhh*hK�ube�transform_messages�]�(j  )��}�(hhh]�h,)��}�(hhh]�h�7Hyperlink target "possible-issues-1" is not referenced.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�K0uh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�=Hyperlink target "alternativessolutions-1" is not referenced.�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�K>uh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�7Hyperlink target "possible-issues-2" is not referenced.�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�KZuh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�=Hyperlink target "alternativessolutions-2" is not referenced.�����}�(hhhj5  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj2  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�Kauh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�7Hyperlink target "possible-issues-3" is not referenced.�����}�(hhhjO  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hjL  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�K�uh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�=Hyperlink target "alternativessolutions-3" is not referenced.�����}�(hhhji  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hjf  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�K�uh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�7Hyperlink target "possible-issues-4" is not referenced.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�K�uh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�=Hyperlink target "alternativessolutions-4" is not referenced.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�K�uh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�7Hyperlink target "possible-issues-5" is not referenced.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�K�uh)j  ubj  )��}�(hhh]�h,)��}�(hhh]�h�=Hyperlink target "alternativessolutions-5" is not referenced.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �source�h*�line�K�uh)j  ube�transformer�N�
decoration�Nhhub.