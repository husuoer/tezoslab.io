���8      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _protocol_environment:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��protocol-environment�u�tagname�h	�line�K�parent�hhh�source��>/home/greg/Tezos/tezos/docs/tutorials/protocol_environment.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Economic protocol sandboxing�h]�h �Text����Economic protocol sandboxing�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(hX*  In Alpha, as in any sound future protocols, updates are approved by
voting. That way, the responsibility of switching to a new protocol code
is the responsibility of voters, and one could argue that it is up to
them to check that the code does not call, for instance, unsafe array
access functions.�h]�h.X*  In Alpha, as in any sound future protocols, updates are approved by
voting. That way, the responsibility of switching to a new protocol code
is the responsibility of voters, and one could argue that it is up to
them to check that the code does not call, for instance, unsafe array
access functions.�����}�(hh=hh;hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(hX^  Yet, we decided to introduce a minimum level of machine checks, by
compiling with a specific compiler that checks that no known-unsafe
function is used. This static form of sandboxing is performed by the
OCaml typechecker: we simply compile protocols in a restricted set of
modules with restricted interfaces that hide any unsafe, non wanted
feature.�h]�h.X^  Yet, we decided to introduce a minimum level of machine checks, by
compiling with a specific compiler that checks that no known-unsafe
function is used. This static form of sandboxing is performed by the
OCaml typechecker: we simply compile protocols in a restricted set of
modules with restricted interfaces that hide any unsafe, non wanted
feature.�����}�(hhKhhIhhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(hX_  Another goal of that specific environment is maintaining a stable OCaml
API for protocol development. Imagine that at some point, the OCaml
standard library changes (a function is added or removed, a type is
changed), then we will be able to upgrade to the new OCaml while still
remaining compatible with past protocols, by providing an adapter layer.�h]�h.X_  Another goal of that specific environment is maintaining a stable OCaml
API for protocol development. Imagine that at some point, the OCaml
standard library changes (a function is added or removed, a type is
changed), then we will be able to upgrade to the new OCaml while still
remaining compatible with past protocols, by providing an adapter layer.�����}�(hhYhhWhhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(h�=Here is a quick description of each file in this environment:�h]�h.�=Here is a quick description of each file in this environment:�����}�(hhghhehhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(hX�  Files ``array.mli``, ``buffer.mli``, ``bytes.mli``, ``format.mli``,
``int32.mli``, ``int64.mli``, ``list.mli``, ``map.mli``,
``pervasives.mli``, ``set.mli`` and ``string.mli`` are stripped down
interfaces to the OCaml standard library modules. The removed
elements are: effects on toplevel references or channels, unsafe
functions, functions that are known sources of bugs, and anything
deprecated.�h]�h:)��}�(hX�  Files ``array.mli``, ``buffer.mli``, ``bytes.mli``, ``format.mli``,
``int32.mli``, ``int64.mli``, ``list.mli``, ``map.mli``,
``pervasives.mli``, ``set.mli`` and ``string.mli`` are stripped down
interfaces to the OCaml standard library modules. The removed
elements are: effects on toplevel references or channels, unsafe
functions, functions that are known sources of bugs, and anything
deprecated.�h]�(h.�Files �����}�(h�Files �hh~ubh �literal���)��}�(h�``array.mli``�h]�h.�	array.mli�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�, �����}�(h�, �hh~ubh�)��}�(h�``buffer.mli``�h]�h.�
buffer.mli�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�, �����}�(h�, �hh~ubh�)��}�(h�``bytes.mli``�h]�h.�	bytes.mli�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�, �����}�(h�, �hh~ubh�)��}�(h�``format.mli``�h]�h.�
format.mli�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�,
�����}�(h�,
�hh~ubh�)��}�(h�``int32.mli``�h]�h.�	int32.mli�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�, �����}�(h�, �hh~ubh�)��}�(h�``int64.mli``�h]�h.�	int64.mli�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�, �����}�(h�, �hh~ubh�)��}�(h�``list.mli``�h]�h.�list.mli�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�, �����}�(h�, �hh~ubh�)��}�(h�``map.mli``�h]�h.�map.mli�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�,
�����}�(hh�hh~ubh�)��}�(h�``pervasives.mli``�h]�h.�pervasives.mli�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�, �����}�(hh�hh~ubh�)��}�(h�``set.mli``�h]�h.�set.mli�����}�(hhhj2  ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.� and �����}�(h� and �hh~ubh�)��}�(h�``string.mli``�h]�h.�
string.mli�����}�(hhhjE  ubah}�(h]�h]�h]�h]�h]�uhh�hh~ubh.�� are stripped down
interfaces to the OCaml standard library modules. The removed
elements are: effects on toplevel references or channels, unsafe
functions, functions that are known sources of bugs, and anything
deprecated.�����}�(h�� are stripped down
interfaces to the OCaml standard library modules. The removed
elements are: effects on toplevel references or channels, unsafe
functions, functions that are known sources of bugs, and anything
deprecated.�hh~ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhhzubah}�(h]�h]�h]�h]�h]�uhhxhhuhhh h!hNubhy)��}�(h��As we removed polymorphic comparison operators, ``compare.mli``
implements monomorphic operators for standard OCaml and Tezos types.
An example use is ``Compare.Int.(3 = 4)`` instead of plain OCaml
``(3 = 4)``.�h]�h:)��}�(h��As we removed polymorphic comparison operators, ``compare.mli``
implements monomorphic operators for standard OCaml and Tezos types.
An example use is ``Compare.Int.(3 = 4)`` instead of plain OCaml
``(3 = 4)``.�h]�(h.�0As we removed polymorphic comparison operators, �����}�(h�0As we removed polymorphic comparison operators, �hjh  ubh�)��}�(h�``compare.mli``�h]�h.�compare.mli�����}�(hhhjq  ubah}�(h]�h]�h]�h]�h]�uhh�hjh  ubh.�X
implements monomorphic operators for standard OCaml and Tezos types.
An example use is �����}�(h�X
implements monomorphic operators for standard OCaml and Tezos types.
An example use is �hjh  ubh�)��}�(h�``Compare.Int.(3 = 4)``�h]�h.�Compare.Int.(3 = 4)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hjh  ubh.� instead of plain OCaml
�����}�(h� instead of plain OCaml
�hjh  ubh�)��}�(h�``(3 = 4)``�h]�h.�(3 = 4)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hjh  ubh.�.�����}�(h�.�hjh  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK"hjd  ubah}�(h]�h]�h]�h]�h]�uhhxhhuhhh h!hNubhy)��}�(h��Files ``lwt*`` is the stripped down interface to Lwt, of which we
removed any non deterministic functions, since we only use Lwt for
asynchronous access to the storage.�h]�h:)��}�(h��Files ``lwt*`` is the stripped down interface to Lwt, of which we
removed any non deterministic functions, since we only use Lwt for
asynchronous access to the storage.�h]�(h.�Files �����}�(h�Files �hj�  ubh�)��}�(h�``lwt*``�h]�h.�lwt*�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�� is the stripped down interface to Lwt, of which we
removed any non deterministic functions, since we only use Lwt for
asynchronous access to the storage.�����}�(h�� is the stripped down interface to Lwt, of which we
removed any non deterministic functions, since we only use Lwt for
asynchronous access to the storage.�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK&hj�  ubah}�(h]�h]�h]�h]�h]�uhhxhhuhhh h!hNubhy)��}�(hX  Files ``data_encoding.mli``, ``error_monad.mli``, ``mBytes.mli``,
``hash.mli``, ``base58.mli``, ``blake2B.mli``, ``ed25519.mli``,
``hex_encode.mli``, ``json.mli``, ``time.mli``, ``z.mli``,
``micheline.mli`` and files ``RPC_*`` are stripped down versions of
the Tezos standard library.�h]�h:)��}�(hX  Files ``data_encoding.mli``, ``error_monad.mli``, ``mBytes.mli``,
``hash.mli``, ``base58.mli``, ``blake2B.mli``, ``ed25519.mli``,
``hex_encode.mli``, ``json.mli``, ``time.mli``, ``z.mli``,
``micheline.mli`` and files ``RPC_*`` are stripped down versions of
the Tezos standard library.�h]�(h.�Files �����}�(h�Files �hj�  ubh�)��}�(h�``data_encoding.mli``�h]�h.�data_encoding.mli�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(h�, �hj�  ubh�)��}�(h�``error_monad.mli``�h]�h.�error_monad.mli�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(h�, �hj�  ubh�)��}�(h�``mBytes.mli``�h]�h.�
mBytes.mli�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�,
�����}�(h�,
�hj�  ubh�)��}�(h�``hash.mli``�h]�h.�hash.mli�����}�(hhhj(  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(h�, �hj�  ubh�)��}�(h�``base58.mli``�h]�h.�
base58.mli�����}�(hhhj;  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(h�, �hj�  ubh�)��}�(h�``blake2B.mli``�h]�h.�blake2B.mli�����}�(hhhjN  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(h�, �hj�  ubh�)��}�(h�``ed25519.mli``�h]�h.�ed25519.mli�����}�(hhhja  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�,
�����}�(h�,
�hj�  ubh�)��}�(h�``hex_encode.mli``�h]�h.�hex_encode.mli�����}�(hhhjt  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(h�, �hj�  ubh�)��}�(h�``json.mli``�h]�h.�json.mli�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(h�, �hj�  ubh�)��}�(h�``time.mli``�h]�h.�time.mli�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(hj  hj�  ubh�)��}�(h�	``z.mli``�h]�h.�z.mli�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�,
�����}�(hj'  hj�  ubh�)��}�(h�``micheline.mli``�h]�h.�micheline.mli�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.� and files �����}�(h� and files �hj�  ubh�)��}�(h�	``RPC_*``�h]�h.�RPC_*�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�: are stripped down versions of
the Tezos standard library.�����}�(h�: are stripped down versions of
the Tezos standard library.�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK)hj�  ubah}�(h]�h]�h]�h]�h]�uhhxhhuhhh h!hNubhy)��}�(h��Files ``tezos_data.mli``, ``context.mli``, ``fitness.mli`` and
``updater.mli`` are interfaces to the shell’s data definitions and
storage accessors that are accessible to the protocol.�h]�h:)��}�(h��Files ``tezos_data.mli``, ``context.mli``, ``fitness.mli`` and
``updater.mli`` are interfaces to the shell’s data definitions and
storage accessors that are accessible to the protocol.�h]�(h.�Files �����}�(h�Files �hj�  ubh�)��}�(h�``tezos_data.mli``�h]�h.�tezos_data.mli�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(h�, �hj�  ubh�)��}�(h�``context.mli``�h]�h.�context.mli�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, �����}�(hj  hj�  ubh�)��}�(h�``fitness.mli``�h]�h.�fitness.mli�����}�(hhhj"  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.� and
�����}�(h� and
�hj�  ubh�)��}�(h�``updater.mli``�h]�h.�updater.mli�����}�(hhhj5  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�l are interfaces to the shell’s data definitions and
storage accessors that are accessible to the protocol.�����}�(h�l are interfaces to the shell’s data definitions and
storage accessors that are accessible to the protocol.�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK.hj�  ubah}�(h]�h]�h]�h]�h]�uhhxhhuhhh h!hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhhsh h!hKhh$hhubeh}�(h]�(�economic-protocol-sandboxing�heh]�h]�(�economic protocol sandboxing��protocol_environment�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�jb  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(jb  hja  j^  u�	nametypes�}�(jb  �ja  Nuh}�(hh$j^  h$u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h:)��}�(hhh]�h.�:Hyperlink target "protocol-environment" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj�  uba�transformer�N�
decoration�Nhhub.