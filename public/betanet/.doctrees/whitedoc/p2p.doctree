���Y      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _p2p:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��p2p�u�tagname�h	�line�K�parent�hhh�source��,/home/greg/Tezos/tezos/docs/whitedoc/p2p.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�The peer-to-peer layer�h]�h �Text����The peer-to-peer layer�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(h��This document explains the inner workings of the peer-to-peer layer of
the Tezos shell. This part is in charge of establishing and
maintaining network connections with other nodes (gossip).�h]�h.��This document explains the inner workings of the peer-to-peer layer of
the Tezos shell. This part is in charge of establishing and
maintaining network connections with other nodes (gossip).�����}�(hh=hh;hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(hX�  The P2P layer is instantiated by the node. It is parametrized by the
type of messages that are exchanged over the network (to allow
different P2P protocol versions/extensions), and the type of metadata
associated to each peer. The latter is useful to compute a score for
each peer that reflects the level of trust we have in it. Different
policies can be used when communicating with peers with different
score values.�h]�h.X�  The P2P layer is instantiated by the node. It is parametrized by the
type of messages that are exchanged over the network (to allow
different P2P protocol versions/extensions), and the type of metadata
associated to each peer. The latter is useful to compute a score for
each peer that reflects the level of trust we have in it. Different
policies can be used when communicating with peers with different
score values.�����}�(hhKhhIhhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK
hh$hhubh:)��}�(h��The P2P layer is comprised of a pool of connections, a set of
operations on those connections, and a set of workers following the
worker pattern pervasively used in the code base.�h]�h.��The P2P layer is comprised of a pool of connections, a set of
operations on those connections, and a set of workers following the
worker pattern pervasively used in the code base.�����}�(hhYhhWhhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(h�[The P2P layer is packaged in :package:`tezos-p2p`, which has
documentation for all modules.�h]�(h.�The P2P layer is packaged in �����}�(h�The P2P layer is packaged in �hhehhh NhNubh �	reference���)��}�(h�:package:`tezos-p2p`�h]�h.�	tezos-p2p�����}�(hhhhpubah}�(h]�h]�h]�h]�h]��refuri��6https://gitlab.com/tezos/tezos/tree/master/src/lib_p2p�uhhnhheubh.�*, which has
documentation for all modules.�����}�(h�*, which has
documentation for all modules.�hhehhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh#)��}�(hhh]�(h()��}�(h�General operation�h]�h.�General operation�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hKubh#)��}�(hhh]�(h()��}�(h�I/O Scheduling�h]�h.�I/O Scheduling�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hKubh:)��}�(hXc  The P2P layer uses a scheduling mechanism in order to control its
bandwidth usage as well as implementing different policies
(e.g. read/write quotas) to different peers. For now, each peer is
granted a fair share of the global allocated bandwidth, but it is
planned for the individual allocated bandwidth to each peer to be a
function of the peer's score.�h]�h.Xe  The P2P layer uses a scheduling mechanism in order to control its
bandwidth usage as well as implementing different policies
(e.g. read/write quotas) to different peers. For now, each peer is
granted a fair share of the global allocated bandwidth, but it is
planned for the individual allocated bandwidth to each peer to be a
function of the peer’s score.�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�hhubeh}�(h]��i-o-scheduling�ah]�h]��i/o scheduling�ah]�h]�uhh"hh�hhh h!hKubh#)��}�(hhh]�(h()��}�(h�
Encryption�h]�h.�
Encryption�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hK'ubh:)��}�(h��The connection between each peer is encrypted using `NaCl`
authenticated-encryption `API <http://nacl.cr.yp.to/box.html>`__. This
is done to provide an additional level of security and tamper-proof
guarantees in the communication between peers.�h]�(h.�4The connection between each peer is encrypted using �����}�(h�4The connection between each peer is encrypted using �hh�hhh NhNubh �title_reference���)��}�(h�`NaCl`�h]�h.�NaCl�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh.�
authenticated-encryption �����}�(h�
authenticated-encryption �hh�hhh NhNubho)��}�(h�'`API <http://nacl.cr.yp.to/box.html>`__�h]�h.�API�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name��API��refuri��http://nacl.cr.yp.to/box.html�uhhnhh�ubh.�y. This
is done to provide an additional level of security and tamper-proof
guarantees in the communication between peers.�����}�(h�y. This
is done to provide an additional level of security and tamper-proof
guarantees in the communication between peers.�hh�hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK)hh�hhubeh}�(h]��
encryption�ah]�h]��
encryption�ah]�h]�uhh"hh�hhh h!hK'ubh#)��}�(hhh]�(h()��}�(h�Message queues�h]�h.�Message queues�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj  hhh h!hK/ubh:)��}�(hXf  On top of basic I/O scheduling, two finite-size typed message queues
are used to store incoming (resp. outgoing) messages for each
peer. This further restricts the speed at which communication is
possible with a peer; when a queue is full, it is not possible to read
(resp. write) an additional message. The high-level
`P2p_socket.connection
<../api/odoc/tezos-p2p/Tezos_p2p/P2p_socket/index.html#type-connection>`__
type by the P2P layer is basically a UNIX socket upgraded with I/O
scheduling, peer metadata, cryptographic keys and two messages queues
operated by dedicated workers which operate on those queues.�h]�(h.X?  On top of basic I/O scheduling, two finite-size typed message queues
are used to store incoming (resp. outgoing) messages for each
peer. This further restricts the speed at which communication is
possible with a peer; when a queue is full, it is not possible to read
(resp. write) an additional message. The high-level
�����}�(hX?  On top of basic I/O scheduling, two finite-size typed message queues
are used to store incoming (resp. outgoing) messages for each
peer. This further restricts the speed at which communication is
possible with a peer; when a queue is full, it is not possible to read
(resp. write) an additional message. The high-level
�hj(  hhh NhNubho)��}�(h�a`P2p_socket.connection
<../api/odoc/tezos-p2p/Tezos_p2p/P2p_socket/index.html#type-connection>`__�h]�h.�P2p_socket.connection�����}�(hhhj1  ubah}�(h]�h]�h]�h]�h]��name��P2p_socket.connection�j  �E../api/odoc/tezos-p2p/Tezos_p2p/P2p_socket/index.html#type-connection�uhhnhj(  ubh.��
type by the P2P layer is basically a UNIX socket upgraded with I/O
scheduling, peer metadata, cryptographic keys and two messages queues
operated by dedicated workers which operate on those queues.�����}�(h��
type by the P2P layer is basically a UNIX socket upgraded with I/O
scheduling, peer metadata, cryptographic keys and two messages queues
operated by dedicated workers which operate on those queues.�hj(  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK1hj  hhubeh}�(h]��message-queues�ah]�h]��message queues�ah]�h]�uhh"hh�hhh h!hK/ubh#)��}�(hhh]�(h()��}�(h�Pool of connections�h]�h.�Pool of connections�����}�(hjZ  hjX  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hjU  hhh h!hK=ubh:)��}�(hX�  All the above modules are used in `P2p_pool
<../api/odoc/tezos-p2p/Tezos_p2p/P2p_pool/index.html>`__, which
constitutes the core of the P2P layer, together with the worker
processes described below. It comprises various tables of connections
as well as methods to query them, also connections are extended with
another message queue where lower level messages (like responses to
ping) are filtered out and only application-level messages are kept.�h]�(h.�"All the above modules are used in �����}�(h�"All the above modules are used in �hjf  hhh NhNubho)��}�(h�B`P2p_pool
<../api/odoc/tezos-p2p/Tezos_p2p/P2p_pool/index.html>`__�h]�h.�P2p_pool�����}�(hhhjo  ubah}�(h]�h]�h]�h]�h]��name��P2p_pool�j  �3../api/odoc/tezos-p2p/Tezos_p2p/P2p_pool/index.html�uhhnhjf  ubh.X[  , which
constitutes the core of the P2P layer, together with the worker
processes described below. It comprises various tables of connections
as well as methods to query them, also connections are extended with
another message queue where lower level messages (like responses to
ping) are filtered out and only application-level messages are kept.�����}�(hX[  , which
constitutes the core of the P2P layer, together with the worker
processes described below. It comprises various tables of connections
as well as methods to query them, also connections are extended with
another message queue where lower level messages (like responses to
ping) are filtered out and only application-level messages are kept.�hjf  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK?hjU  hhubh:)��}�(h��The main entry point of the P2P layer is in module `P2p
<../api/odoc/tezos-p2p/Tezos_p2p/P2p/index.html>`__. See below
for a description of workers acting onto the P2P layer.�h]�(h.�3The main entry point of the P2P layer is in module �����}�(h�3The main entry point of the P2P layer is in module �hj�  hhh NhNubho)��}�(h�8`P2p
<../api/odoc/tezos-p2p/Tezos_p2p/P2p/index.html>`__�h]�h.�P2p�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��P2p�j  �.../api/odoc/tezos-p2p/Tezos_p2p/P2p/index.html�uhhnhj�  ubh.�C. See below
for a description of workers acting onto the P2P layer.�����}�(h�C. See below
for a description of workers acting onto the P2P layer.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKGhjU  hhubeh}�(h]��pool-of-connections�ah]�h]��pool of connections�ah]�h]�uhh"hh�hhh h!hK=ubeh}�(h]��general-operation�ah]�h]��general operation�ah]�h]�uhh"hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�Welcome worker�h]�h.�Welcome worker�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj�  hhh h!hKLubh:)��}�(hX�  The welcome worker is responsible for accepting incoming connections
and register them into the pool of connections managed by the P2P
layer. It basically runs the ``accept(2)`` syscall and call
`P2p_pool.accept
<../api/odoc/tezos-p2p/Tezos_p2p/P2p_pool/index.html#val-accept>`__ so
that it is made aware of an incoming connection. From there, the pool
will decide how this new connection must be handled.�h]�(h.��The welcome worker is responsible for accepting incoming connections
and register them into the pool of connections managed by the P2P
layer. It basically runs the �����}�(h��The welcome worker is responsible for accepting incoming connections
and register them into the pool of connections managed by the P2P
layer. It basically runs the �hj�  hhh NhNubh �literal���)��}�(h�``accept(2)``�h]�h.�	accept(2)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh.� syscall and call
�����}�(h� syscall and call
�hj�  hhh NhNubho)��}�(h�T`P2p_pool.accept
<../api/odoc/tezos-p2p/Tezos_p2p/P2p_pool/index.html#val-accept>`__�h]�h.�P2p_pool.accept�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��P2p_pool.accept�j  �>../api/odoc/tezos-p2p/Tezos_p2p/P2p_pool/index.html#val-accept�uhhnhj�  ubh.�~ so
that it is made aware of an incoming connection. From there, the pool
will decide how this new connection must be handled.�����}�(h�~ so
that it is made aware of an incoming connection. From there, the pool
will decide how this new connection must be handled.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKNhj�  hhubh#)��}�(hhh]�(h()��}�(h�{Black, While, Grey}lists�h]�h.�{Black, While, Grey}lists�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj  hhh h!hKWubh:)��}�(hXr  The welcome worker takes care of filtering all incoming connections using two
static lists of addresses handled either by ``tezos-admin-client`` and a system
table that is handled automatically by the p2p layer. The node admin can block
or whitelist individual ip addresses, while the p2p layer is in charge of
temporarily banning ip addresses and peers who misbehave. The delay to remove
an ip address from the greylist table is defined by the configuration variable
``greylist_timeout``, while peers that are greylisted are periodically removed.
The node admin can also flush greylist tables with the ``tezos-admin-client``.�h]�(h.�zThe welcome worker takes care of filtering all incoming connections using two
static lists of addresses handled either by �����}�(h�zThe welcome worker takes care of filtering all incoming connections using two
static lists of addresses handled either by �hj  hhh NhNubj�  )��}�(h�``tezos-admin-client``�h]�h.�tezos-admin-client�����}�(hhhj%  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh.XD   and a system
table that is handled automatically by the p2p layer. The node admin can block
or whitelist individual ip addresses, while the p2p layer is in charge of
temporarily banning ip addresses and peers who misbehave. The delay to remove
an ip address from the greylist table is defined by the configuration variable
�����}�(hXD   and a system
table that is handled automatically by the p2p layer. The node admin can block
or whitelist individual ip addresses, while the p2p layer is in charge of
temporarily banning ip addresses and peers who misbehave. The delay to remove
an ip address from the greylist table is defined by the configuration variable
�hj  hhh NhNubj�  )��}�(h�``greylist_timeout``�h]�h.�greylist_timeout�����}�(hhhj8  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh.�s, while peers that are greylisted are periodically removed.
The node admin can also flush greylist tables with the �����}�(h�s, while peers that are greylisted are periodically removed.
The node admin can also flush greylist tables with the �hj  hhh NhNubj�  )��}�(h�``tezos-admin-client``�h]�h.�tezos-admin-client�����}�(hhhjK  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh.�.�����}�(h�.�hj  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKYhj  hhubeh}�(h]��black-while-grey-lists�ah]�h]��{black, while, grey}lists�ah]�h]�uhh"hj�  hhh h!hKWubeh}�(h]��welcome-worker�ah]�h]��welcome worker�ah]�h]�uhh"hh$hhh h!hKLubh#)��}�(hhh]�(h()��}�(h�Maintenance worker�h]�h.�Maintenance worker�����}�(hjy  hjw  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hjt  hhh h!hKcubh:)��}�(hX-  The maintenance worker is in charge of establishing an appropriate
number of connections with other nodes in order to guarantee a
realistic view of the state of the blockchain. It is created with a
set of targets to reach regarding the desired amount of peers it needs
to keep an active connection to.�h]�h.X-  The maintenance worker is in charge of establishing an appropriate
number of connections with other nodes in order to guarantee a
realistic view of the state of the blockchain. It is created with a
set of targets to reach regarding the desired amount of peers it needs
to keep an active connection to.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKehjt  hhubh:)��}�(h�[At the pool level, the minimum (resp. maximum) acceptable number of
connections is defined.�h]�h.�[At the pool level, the minimum (resp. maximum) acceptable number of
connections is defined.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKkhjt  hhubh:)��}�(h��At the maintenance worker level, two other sets of thresholds are
defined: ``target`` (min and max) and ``threshold`` (min and max).�h]�(h.�KAt the maintenance worker level, two other sets of thresholds are
defined: �����}�(h�KAt the maintenance worker level, two other sets of thresholds are
defined: �hj�  hhh NhNubj�  )��}�(h�
``target``�h]�h.�target�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh.� (min and max) and �����}�(h� (min and max) and �hj�  hhh NhNubj�  )��}�(h�``threshold``�h]�h.�	threshold�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh.� (min and max).�����}�(h� (min and max).�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKnhjt  hhubh:)��}�(h�+Given these bounds, the maintenance worker:�h]�h.�+Given these bounds, the maintenance worker:�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKqhjt  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h��Will be triggered every two minutes, when asked by the shell, or
when the minimum or maximum number of acceptable connections is
reached, whichever happens first.
�h]�h:)��}�(h��Will be triggered every two minutes, when asked by the shell, or
when the minimum or maximum number of acceptable connections is
reached, whichever happens first.�h]�h.��Will be triggered every two minutes, when asked by the shell, or
when the minimum or maximum number of acceptable connections is
reached, whichever happens first.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKshj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhh h!hNubj�  )��}�(hX  Will perform the following actions when triggered: if the number of
connections is above ``max_threshold``, it will kill connections
randomly until it reaches ``max_target`` connections. If the number of
connections is below ``min_threshold``, it will attempt to connect to
peers until it reaches at least ``min_target`` connections (and never
more than ``max_target`` connections).
�h]�h:)��}�(hX~  Will perform the following actions when triggered: if the number of
connections is above ``max_threshold``, it will kill connections
randomly until it reaches ``max_target`` connections. If the number of
connections is below ``min_threshold``, it will attempt to connect to
peers until it reaches at least ``min_target`` connections (and never
more than ``max_target`` connections).�h]�(h.�YWill perform the following actions when triggered: if the number of
connections is above �����}�(h�YWill perform the following actions when triggered: if the number of
connections is above �hj  ubj�  )��}�(h�``max_threshold``�h]�h.�max_threshold�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh.�5, it will kill connections
randomly until it reaches �����}�(h�5, it will kill connections
randomly until it reaches �hj  ubj�  )��}�(h�``max_target``�h]�h.�
max_target�����}�(hhhj#  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh.�4 connections. If the number of
connections is below �����}�(h�4 connections. If the number of
connections is below �hj  ubj�  )��}�(h�``min_threshold``�h]�h.�min_threshold�����}�(hhhj6  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh.�@, it will attempt to connect to
peers until it reaches at least �����}�(h�@, it will attempt to connect to
peers until it reaches at least �hj  ubj�  )��}�(h�``min_target``�h]�h.�
min_target�����}�(hhhjI  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh.�" connections (and never
more than �����}�(h�" connections (and never
more than �hj  ubj�  )��}�(h�``max_target``�h]�h.�
max_target�����}�(hhhj\  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh.� connections).�����}�(h� connections).�hj  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKwhj  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhh h!hNubeh}�(h]�h]�h]�h]�h]��bullet��*�uhj�  h h!hKshjt  hhubh:)��}�(h��The maintenance worker is also in charge of periodically run the
greylists GC functions to unban ip addresses from the greylist.�h]�h.��The maintenance worker is also in charge of periodically run the
greylists GC functions to unban ip addresses from the greylist.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK~hjt  hhubeh}�(h]��maintenance-worker�ah]�h]��maintenance worker�ah]�h]�uhh"hh$hhh h!hKcubeh}�(h]�(�the-peer-to-peer-layer�heh]�h]�(�the peer-to-peer layer��p2p�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(j�  hj�  j�  j�  j�  h�h�j  j  jR  jO  j�  j�  jq  jn  ji  jf  j�  j�  u�	nametypes�}�(j�  �j�  Nj�  Nh�Nj  NjR  Nj�  Njq  Nji  Nj�  Nuh}�(hh$j�  h$j�  h�h�h�j  h�jO  j  j�  jU  jn  j�  jf  j  j�  jt  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h:)��}�(hhh]�h.�)Hyperlink target "p2p" is not referenced.�����}�(hhhj)  ubah}�(h]�h]�h]�h]�h]�uhh9hj&  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj$  uba�transformer�N�
decoration�Nhhub.