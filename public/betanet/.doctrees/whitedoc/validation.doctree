��&_      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _validation:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��
validation�u�tagname�h	�line�K�parent�hhh�source��3/home/greg/Tezos/tezos/docs/whitedoc/validation.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�The validation subsystem�h]�h �Text����The validation subsystem�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(hX�  This document explains the inner workings of the validation subsystem
of the Tezos shell, that sits between the peer-to-peer layer and the
economic protocol. This part is in charge of validating chains, blocks
and operations that come from the network, and deciding whether they
are worthy to propagate. It is composed of three main parts: the
:ref:`validator<validator_component>`, the
:ref:`prevalidator<prevalidator_component>`, and
the :ref:`distributed DB<DDB_component>`.�h]�(h.XX  This document explains the inner workings of the validation subsystem
of the Tezos shell, that sits between the peer-to-peer layer and the
economic protocol. This part is in charge of validating chains, blocks
and operations that come from the network, and deciding whether they
are worthy to propagate. It is composed of three main parts: the
�����}�(hXX  This document explains the inner workings of the validation subsystem
of the Tezos shell, that sits between the peer-to-peer layer and the
economic protocol. This part is in charge of validating chains, blocks
and operations that come from the network, and deciding whether they
are worthy to propagate. It is composed of three main parts: the
�hh;hhh NhNub�sphinx.addnodes��pending_xref���)��}�(h�%:ref:`validator<validator_component>`�h]�h �inline���)��}�(hhIh]�h.�	validator�����}�(hhhhMubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhKhhGubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hX�refexplicit���	reftarget��validator_component��refdoc��whitedoc/validation��refwarn��uhhEh h!hKhh;ubh.�, the
�����}�(h�, the
�hh;hhh NhNubhF)��}�(h�+:ref:`prevalidator<prevalidator_component>`�h]�hL)��}�(hhsh]�h.�prevalidator�����}�(hhhhuubah}�(h]�h]�(hW�std��std-ref�eh]�h]�h]�uhhKhhqubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h�refexplicit��hg�prevalidator_component�hihjhk�uhhEh h!hKhh;ubh.�
, and
the �����}�(h�
, and
the �hh;hhh NhNubhF)��}�(h�$:ref:`distributed DB<DDB_component>`�h]�hL)��}�(hh�h]�h.�distributed DB�����}�(hhhh�ubah}�(h]�h]�(hW�std��std-ref�eh]�h]�h]�uhhKhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��hg�ddb_component�hihjhk�uhhEh h!hKhh;ubh.�.�����}�(h�.�hh;hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(h�|Tezos validation diagram|�h]�h �image���)��}�(h�image:: validation.svg�h]�h}�(h]�h]�h]�h]�h]��alt��Tezos validation diagram��uri��whitedoc/validation.svg��
candidates�}��*�h�suhh�h h!hKxhh�hhubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh#)��}�(hhh]�(h()��}�(h�	Validator�h]�h.�	Validator�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hKubh
)��}�(h�.. _validator_component:�h]�h}�(h]�h]�h]�h]�h]�h�validator-component�uhh	hKhh�hhh h!ubh:)��}�(h��The validator is the component responsible for checking that blocks
coming from the network or a baker are valid, w.r.t. the rules defined
by the economic protocol, and for selecting the block that it
considers to be the current head of the blockchain.�h]�h.��The validator is the component responsible for checking that blocks
coming from the network or a baker are valid, w.r.t. the rules defined
by the economic protocol, and for selecting the block that it
considers to be the current head of the blockchain.�����}�(hh�hh�hhh NhNubah}�(h]�h�ah]�h]��validator_component�ah]�h]�uhh9h h!hKhh�hh�expect_referenced_by_name�}�j  h�s�expect_referenced_by_id�}�h�h�subh:)��}�(h��The validator is written as a collection of workers: local event loops
communicating with each other via message passing. Workers are spawned
and killed dynamically, according to connected peers, incoming blocks
to validate, and active (test)chains.�h]�h.��The validator is written as a collection of workers: local event loops
communicating with each other via message passing. Workers are spawned
and killed dynamically, according to connected peers, incoming blocks
to validate, and active (test)chains.�����}�(hj  hj	  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�hhubh:)��}�(hX�  A *chain validator* worker is launched by the validator for each
*chain* that it considers alive. Each chain validator is responsible for
handling blocks that belong to this chain, and select the best head for
this chain. A main chain validator is spawned for the main chain that
starts at the genesis, a second one when there is an active test
chain. Forking a chain is decided from within the economic protocol.  In
version Alpha, this is only used to try new protocols before self
amending the main chain.�h]�(h.�A �����}�(h�A �hj  hhh NhNubh �emphasis���)��}�(h�*chain validator*�h]�h.�chain validator�����}�(hhhj"  ubah}�(h]�h]�h]�h]�h]�uhj   hj  ubh.�. worker is launched by the validator for each
�����}�(h�. worker is launched by the validator for each
�hj  hhh NhNubj!  )��}�(h�*chain*�h]�h.�chain�����}�(hhhj5  ubah}�(h]�h]�h]�h]�h]�uhj   hj  ubh.X�   that it considers alive. Each chain validator is responsible for
handling blocks that belong to this chain, and select the best head for
this chain. A main chain validator is spawned for the main chain that
starts at the genesis, a second one when there is an active test
chain. Forking a chain is decided from within the economic protocol.  In
version Alpha, this is only used to try new protocols before self
amending the main chain.�����}�(hX�   that it considers alive. Each chain validator is responsible for
handling blocks that belong to this chain, and select the best head for
this chain. A main chain validator is spawned for the main chain that
starts at the genesis, a second one when there is an active test
chain. Forking a chain is decided from within the economic protocol.  In
version Alpha, this is only used to try new protocols before self
amending the main chain.�hj  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�hhubh:)��}�(hX�  The chain validator spawns one *peer validator* worker per connected
peer. This set is updated, grown or shrunk on the fly, according to the
connections and disconnections signals from the peer-to-peer component.
Each peer validator will treat new head proposals from the associated
peer, one at a time, in a loop. In the simple case, when a peer
receives a new head proposal that is a direct successor of the current
local head, it launches a simple *head increment* task: it retrieves
all the operations and triggers a validation of the block. When the
difference between the current head and the examined proposal is
more than one block, mostly during the initial bootstrap phase, the
peer worker launches a *bootstrap pipeline* task.�h]�(h.�The chain validator spawns one �����}�(h�The chain validator spawns one �hjN  hhh NhNubj!  )��}�(h�*peer validator*�h]�h.�peer validator�����}�(hhhjW  ubah}�(h]�h]�h]�h]�h]�uhj   hjN  ubh.X�   worker per connected
peer. This set is updated, grown or shrunk on the fly, according to the
connections and disconnections signals from the peer-to-peer component.
Each peer validator will treat new head proposals from the associated
peer, one at a time, in a loop. In the simple case, when a peer
receives a new head proposal that is a direct successor of the current
local head, it launches a simple �����}�(hX�   worker per connected
peer. This set is updated, grown or shrunk on the fly, according to the
connections and disconnections signals from the peer-to-peer component.
Each peer validator will treat new head proposals from the associated
peer, one at a time, in a loop. In the simple case, when a peer
receives a new head proposal that is a direct successor of the current
local head, it launches a simple �hjN  hhh NhNubj!  )��}�(h�*head increment*�h]�h.�head increment�����}�(hhhjj  ubah}�(h]�h]�h]�h]�h]�uhj   hjN  ubh.�� task: it retrieves
all the operations and triggers a validation of the block. When the
difference between the current head and the examined proposal is
more than one block, mostly during the initial bootstrap phase, the
peer worker launches a �����}�(h�� task: it retrieves
all the operations and triggers a validation of the block. When the
difference between the current head and the examined proposal is
more than one block, mostly during the initial bootstrap phase, the
peer worker launches a �hjN  hhh NhNubj!  )��}�(h�*bootstrap pipeline*�h]�h.�bootstrap pipeline�����}�(hhhj}  ubah}�(h]�h]�h]�h]�h]�uhj   hjN  ubh.� task.�����}�(h� task.�hjN  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK(hh�hhubh:)��}�(hX�  A third scheme is planned (but not yet implemented) for validating
alternative chains: the *multipass validator*. This method is quite more
complex, its goal is to detect erroneous blocks as soon as possible,
without having to download all the chain data. This will work by first
validating the block headers, then the operations that act on the
fitness, and finally the remaining operations. The mechanism is
actually a bit more flexible, and allows for an arbitrary number of
lists of operations. The shell will only consider forks of a given
length, that is exported by the protocol, so that block headers and
operations are validated in the context of an ancestor block that is
in a close enough time window. In version Alpha, the check performed
on block headers is that the baking slots, baker signatures and
timestamp deltas are right. It can also detect too large fitness gaps,
as the fitness difference between two consecutive blocks is bounded in
Alpha. The operations that act on fitness are endorsements, whose
checks consist in verifying the endorsement slots and endorsers'
signatures. For that to be sound, the fork limit is set to not allow
rewinding before the baking and endorsing slots are set.�h]�(h.�[A third scheme is planned (but not yet implemented) for validating
alternative chains: the �����}�(h�[A third scheme is planned (but not yet implemented) for validating
alternative chains: the �hj�  hhh NhNubj!  )��}�(h�*multipass validator*�h]�h.�multipass validator�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj   hj�  ubh.XO  . This method is quite more
complex, its goal is to detect erroneous blocks as soon as possible,
without having to download all the chain data. This will work by first
validating the block headers, then the operations that act on the
fitness, and finally the remaining operations. The mechanism is
actually a bit more flexible, and allows for an arbitrary number of
lists of operations. The shell will only consider forks of a given
length, that is exported by the protocol, so that block headers and
operations are validated in the context of an ancestor block that is
in a close enough time window. In version Alpha, the check performed
on block headers is that the baking slots, baker signatures and
timestamp deltas are right. It can also detect too large fitness gaps,
as the fitness difference between two consecutive blocks is bounded in
Alpha. The operations that act on fitness are endorsements, whose
checks consist in verifying the endorsement slots and endorsers’
signatures. For that to be sound, the fork limit is set to not allow
rewinding before the baking and endorsing slots are set.�����}�(hXM  . This method is quite more
complex, its goal is to detect erroneous blocks as soon as possible,
without having to download all the chain data. This will work by first
validating the block headers, then the operations that act on the
fitness, and finally the remaining operations. The mechanism is
actually a bit more flexible, and allows for an arbitrary number of
lists of operations. The shell will only consider forks of a given
length, that is exported by the protocol, so that block headers and
operations are validated in the context of an ancestor block that is
in a close enough time window. In version Alpha, the check performed
on block headers is that the baking slots, baker signatures and
timestamp deltas are right. It can also detect too large fitness gaps,
as the fitness difference between two consecutive blocks is bounded in
Alpha. The operations that act on fitness are endorsements, whose
checks consist in verifying the endorsement slots and endorsers'
signatures. For that to be sound, the fork limit is set to not allow
rewinding before the baking and endorsing slots are set.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK4hh�hhubh:)��}�(hX  Each of these three peer validator tasks (head increment, bootstrap
pipeline or multipass) will interact with the distributed DB to get
the data they need (block headers and operations). When they have
everything needed for a block, they will call the *block validator*.�h]�(h.��Each of these three peer validator tasks (head increment, bootstrap
pipeline or multipass) will interact with the distributed DB to get
the data they need (block headers and operations). When they have
everything needed for a block, they will call the �����}�(h��Each of these three peer validator tasks (head increment, bootstrap
pipeline or multipass) will interact with the distributed DB to get
the data they need (block headers and operations). When they have
everything needed for a block, they will call the �hj�  hhh NhNubj!  )��}�(h�*block validator*�h]�h.�block validator�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj   hj�  ubh.�.�����}�(hh�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKGhh�hhubh:)��}�(hX�  The *block validator* validates blocks (currently in sequence),
assuming that all the necessary data have already been retrieved from
the peer-to-peer network. When a block is valid, it will notify the
corresponding chain validator, that may update its head. In this case,
the chain validator will propagate this information to its associated
*prevalidator*, and may decide to kill or spawn the test network
according to the protocol's decision.�h]�(h.�The �����}�(h�The �hj�  hhh NhNubj!  )��}�(h�*block validator*�h]�h.�block validator�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj   hj�  ubh.XB   validates blocks (currently in sequence),
assuming that all the necessary data have already been retrieved from
the peer-to-peer network. When a block is valid, it will notify the
corresponding chain validator, that may update its head. In this case,
the chain validator will propagate this information to its associated
�����}�(hXB   validates blocks (currently in sequence),
assuming that all the necessary data have already been retrieved from
the peer-to-peer network. When a block is valid, it will notify the
corresponding chain validator, that may update its head. In this case,
the chain validator will propagate this information to its associated
�hj�  hhh NhNubj!  )��}�(h�*prevalidator*�h]�h.�prevalidator�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj   hj�  ubh.�Z, and may decide to kill or spawn the test network
according to the protocol’s decision.�����}�(h�X, and may decide to kill or spawn the test network
according to the protocol's decision.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKLhh�hhubeh}�(h]��	validator�ah]�h]��	validator�ah]�h]�uhh"hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�Prevalidator�h]�h.�Prevalidator�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj  hhh h!hKUubh
)��}�(h�.. _prevalidator_component:�h]�h}�(h]�h]�h]�h]�h]�h�prevalidator-component�uhh	hKVhj  hhh h!ubh:)��}�(h��To each chain validator is associated a *prevalidator* (this may become
an option in the future, to allow running nodes on machines with less
RAM), that is responsible for the transmission of operations for this
chain over the peer-to-peer network.�h]�(h.�(To each chain validator is associated a �����}�(h�(To each chain validator is associated a �hj2  hhh NhNubj!  )��}�(h�*prevalidator*�h]�h.�prevalidator�����}�(hhhj;  ubah}�(h]�h]�h]�h]�h]�uhj   hj2  ubh.�� (this may become
an option in the future, to allow running nodes on machines with less
RAM), that is responsible for the transmission of operations for this
chain over the peer-to-peer network.�����}�(h�� (this may become
an option in the future, to allow running nodes on machines with less
RAM), that is responsible for the transmission of operations for this
chain over the peer-to-peer network.�hj2  hhh NhNubeh}�(h]�j1  ah]�h]��prevalidator_component�ah]�h]�uhh9h h!hKXhj  hhj  }�jR  j'  sj  }�j1  j'  subh:)��}�(hX  To prevent spam, this prevalidator must select the set of operations
that it considers valid, and the ones that it chooses to broadcast.
This is done by constantly baking a dummy block, floating over the
current head, and growing as new operations are received.�h]�h.X  To prevent spam, this prevalidator must select the set of operations
that it considers valid, and the ones that it chooses to broadcast.
This is done by constantly baking a dummy block, floating over the
current head, and growing as new operations are received.�����}�(hjY  hjW  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK]hj  hhubh:)��}�(h�>Operations that get included can be broadcast unconditionally.�h]�h.�>Operations that get included can be broadcast unconditionally.�����}�(hjg  hje  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKbhj  hhubh:)��}�(hX�  Operations that are included are classified into categories. Some
(such as bad signatures or garbage byte sequences) are dismissed. They
are put in a temporary bounded set for quick rejection, and the peer
that sent it is kicked. Some other operations are temporarily refused:
they come too soon or too late. For instance, in Alpha, contracts have
counters, and operations with counters in the future are classified as
temporarily refused. A malicious peer could easily flood the mempool
with such operations, so they are put in a bounded set. Another
bounded set is also kept for a third kind of non inclusion: operations
that could be valid in another branch.�h]�h.X�  Operations that are included are classified into categories. Some
(such as bad signatures or garbage byte sequences) are dismissed. They
are put in a temporary bounded set for quick rejection, and the peer
that sent it is kicked. Some other operations are temporarily refused:
they come too soon or too late. For instance, in Alpha, contracts have
counters, and operations with counters in the future are classified as
temporarily refused. A malicious peer could easily flood the mempool
with such operations, so they are put in a bounded set. Another
bounded set is also kept for a third kind of non inclusion: operations
that could be valid in another branch.�����}�(hju  hjs  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKdhj  hhubeh}�(h]��prevalidator�ah]�h]��prevalidator�ah]�h]�uhh"hh$hhh h!hKUubh#)��}�(hhh]�(h()��}�(h�Distributed DB�h]�h.�Distributed DB�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj�  hhh h!hKpubh
)��}�(h�.. _DDB_component:�h]�h}�(h]�h]�h]�h]�h]�h�ddb-component�uhh	hKqhj�  hhh h!ubh:)��}�(h��The gathering of resources needed for validation is centralized in the
*distributed db*. This component allocates a slot per requested
resource, whose priority depends on the number of peer validators
requesting it.�h]�(h.�GThe gathering of resources needed for validation is centralized in the
�����}�(h�GThe gathering of resources needed for validation is centralized in the
�hj�  hhh NhNubj!  )��}�(h�*distributed db*�h]�h.�distributed db�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj   hj�  ubh.��. This component allocates a slot per requested
resource, whose priority depends on the number of peer validators
requesting it.�����}�(h��. This component allocates a slot per requested
resource, whose priority depends on the number of peer validators
requesting it.�hj�  hhh NhNubeh}�(h]�j�  ah]�h]��ddb_component�ah]�h]�uhh9h h!hKshj�  hhj  }�j�  j�  sj  }�j�  j�  subh �substitution_definition���)��}�(h�4.. |Tezos validation diagram| image:: validation.svg�h]�h�)��}�(hh�h]�h}�(h]�h]�h]�h]�h]��alt�hΌuri��whitedoc/validation.svg�h�}�h�j�  suhh�hj�  h h!hKxubah}�(h]�h]�h]�h�ah]�h]�uhj�  h h!hKxhj�  hhubeh}�(h]��distributed-db�ah]�h]��distributed db�ah]�h]�uhh"hh$hhh h!hKpubeh}�(h]�(�the-validation-subsystem�heh]�h]�(�the validation subsystem��
validation�eh]�h]�uhh"hhhhh h!hKj  }�j�  hsj  }�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��Tezos validation diagram�j�  s�substitution_names�}��tezos validation diagram�jQ  s�refnames�}��refids�}�(h]�hah�]�h�aj1  ]�j'  aj�  ]�j�  au�nameids�}�(j�  hj�  j�  j  j  j  h�j�  j�  jR  j1  j�  j�  j�  j�  u�	nametypes�}�(j�  �j�  Nj  Nj  �j�  NjR  �j�  Nj�  �uh}�(hh$j�  h$j  h�h�h�j�  j  j1  j2  j�  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h:)��}�(hhh]�h.�0Hyperlink target "validation" is not referenced.�����}�(hhhj~  ubah}�(h]�h]�h]�h]�h]�uhh9hj{  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhjy  ubjz  )��}�(hhh]�h:)��}�(hhh]�h.�9Hyperlink target "validator-component" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h!�line�Kuhjy  ubjz  )��}�(hhh]�h:)��}�(hhh]�h.�<Hyperlink target "prevalidator-component" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h!�line�KVuhjy  ubjz  )��}�(hhh]�h:)��}�(hhh]�h.�3Hyperlink target "ddb-component" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h!�line�Kquhjy  ube�transformer�N�
decoration�Nhhub.