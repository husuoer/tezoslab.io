odoc-1.2.0¦¾   3         °,tezos-stdlib¡1Tezos_stdlib__TagA0$D$2z) ±7#h¦¾      ¾  T  $  °,tezos-stdlib¡1Tezos_stdlib__TagA0$D$2z) ±7#h  	üTags and tag sets. Tags are basically similar to a plain extensible
variant type, but wrapped with metadata that enables them to be printed
generically and combined into tag sets where each tag is either not
present or associated with a specific value. @ 	They are primarily intended for use with the `Logging` module but it
would probably be reasonable to use them for other purposes.@@
  &Buffer069Ò=wFÅqÍðFFë)  8CamlinternalFormatBasics0y®·S¯kDàTVÇ  &Format0S$FQð'GQSÔÓµ  *Pervasives0ìÜå,4(ïzÙ¢ °,tezos-stdlib¡,Tezos_stdlib@0=^<rô ô3¦Ì}ºò  %Uchar0YlIsµÃ{G¹Öt~õÑ@°6src/lib_stdlib/tag.mli	"/builds/tezos/tezos/_build/default0ì²ÅÍdl«ZWÜßõAA À¥9#def  	cType of tag definitions. Analogous to a constructor of an extensible
variant type, but first-class.@@À  @@@@@@@ °¬F#def  
  Define a new tag with a name, printer, and optional documentation string.
This is generative, not applicative, so tag definitions created with
identical names and printers at different times or places will be
different tags! You probably do not want to define a tag in a local
scope unless you have something really tricky in mind. Basically all
the caveats you would have if you wrote  )type t += ' apply.@@²#doc¤&string@²@¤@²@²@¤£I)formatter@²@!a¤$unit@¤¥q8 !a@ °¬x$name @@²@¤ !a@¤,@ °¬#doc²@¤ !a@¤<@ °¬'printer!²@¤/ !a@²@¤£&Format)formatter@²@!a¤C@ °¬²&pp_def  	#Print the name of a tag definition.@@²@¤£&Format)formatter@²@¤R !a@¤]@ À¥Ì!t  	A binding consisting of a tag and value. If a `def` is a constructor
of an extensible variant type, a `t` is a value of that type.@@À@@@@ À§!V` ¤o !a@ !a@¤¥ê@@ °¬î"ppv²@¤£&Format)formatter@²@¤@¤@ ð¢#Key À¥	!tÀ@@@@ À§	!V ¤¨ !a@@¤¥ @@@@@@@ À¥%#set  
  pTag sets. If `t` is an extensible variant type, `set` is a set of `t`s
no two of which have the same constructor. Most ordinary set and map
operations familiar from the Ocaml standard library are provided.
`equal` and `compare` are purposely not provided as there is no
meaningful ordering on tags and their arguments may not even have a
meaningful notion of equality.@@À@@@@@ °¬0%empty¸¤¥6@ °¬:(is_emptyÂ²@¤@¤$bool@ °¬I#memÑ²@¤ß !a@²@¤!@¤@ °¬]#addå²@¤ó !a@²@!a²@¤8@¤;@ °¬t&updateü²@¤
 !a@²@²@¤&option !a@¤ !a@²@¤\@¤_@ °¬)singleton ²@¤. !a@²@!a¤r@ °¬«&remove3²@¤A !a@²@¤@¤@ °¬¿#remG²@¤U !a@²@¤@¤@ À¥Ó&merger[À@@@@ À¨	&mergerc@¨ !a@²@¤t !a@²@¤i !a@²@¤p !a@¤v !a@@ °¬ÿ%merge²@¤¥3@²@¤Õ@²@¤Ù@¤Ü@ À¥'unionerÀ@@@@ À¨	'unioner¥@¨ !a@²@¤¶ !a@²@!a²@!a!a@ °¬5%union½²@¤¥<'@²@¤@²@¤@¤@ °¬K$iterÓ²@²@¤i@¤ê@²@¤$@¤ñ@ °¬`$foldè²@²@¤~@²@!b!b²@¤;@²@!b!b °¬y'for_all²@²@¤@¤@@²@¤R@¤G@ °¬&exists²@²@¤¬@¤U@²@¤g@¤\@ °¬£&filter+²@²@¤Á@¤j@²@¤|@¤@ °¬¸)partition@²@²@¤Ö@¤@²@¤@ ¤@ ¤@@ °¬Ó(cardinal[²@¤¤@¤#int@ °¬â+min_bindingj²@¤³@¤@ °¬ï/min_binding_optw²@¤À@¤v ¤@@ °¬ +max_binding²@¤Ñ@¤ @ °¬/max_binding_opt²@¤Þ@¤ ¤1@@ °¬&choose¦²@¤ï@¤>@ °¬+*choose_opt³²@¤ü@¤² ¤O@@ °¬<%splitÄ²@¤Ò !a@²@¤@ ¤@ ¤Ð !a@ ¤$@@ °¬](find_optå²@¤ó !a@²@¤5@¤ë !a@ °¬t$findü²@¤
 !a@²@¤L@¤ !a@ °¬#get²@¤! !a@²@¤c@!a °¬*find_first&²@²@¤¨¢§¦!t@¤h@²@¤z@¤É@ °¬¶.find_first_opt>²@²@¤¨!t@¤~@²@¤@¤F ¤ã@@ °¬Ð)find_lastX²@²@¤¨2!t@¤@²@¤ª@¤ù@ °¬æ-find_last_optn²@²@¤¨H!t@¤®@²@¤À@¤v ¤@@ °¬ #map²@²@¤@¤!@²@¤Ù@¤Ü@ °¬$mapi²@²@¤3@¤6@²@¤î@¤ñ@ °¬*&pp_set²²@¤£&Format)formatter@²@¤@¤Î@ ð¢=#DSL  	²DSL for logging messages. Opening this locally makes it easy to supply a number
of semantic tags for a log event while using their values in the human-readable
text. For example: @ 
  !      lwt_log_info Tag.DSL.(fun f ->
          f "request for operations %a:%d from peer %a timed out."
          -% t event "request_operations_timeout"
          -% a Block_hash.Logging.tag bh
          -% s operations_index_tag n
          -% a P2p_peer.Id.Logging.tag pipeline.peer_id)@@ À¥#argÕÀ  @@  @@  @@  @@@@@@@ °¬ !a  	QUse a semantic tag with a `%a` format, supplying the pretty printer from the tag.@@²@¤ö !v@²@!v¤¥4# ²@²@!b²@!v!c²@!v!d !b !c !d@ °¬P!s  	FUse a semantic tag with ordinary formats such as `%s`, `%d`, and `%f`.@@²@¤& !v@²@!v¤0 ²@!v!d !b !c !d@ °¬v!t  	,Supply a semantic tag without formatting it.@@²@¤L !v@²@!v¤V !d !b !c !d@ °¬$(-%)  	4Perform the actual application of a tag to a format.@@²@²$tags¤®@!a²@¤z !a ¤£&Format)formatter@ ¤@ !d@²$tags¤Ë@!d@@@@â@@