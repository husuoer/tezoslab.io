��Ɗ      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _entering_alpha:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��entering-alpha�u�tagname�h	�line�K�parent�hhh�source��8/home/greg/Tezos/tezos/docs/tutorials/entering_alpha.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�#How to start reading protocol Alpha�h]�h �Text����#How to start reading protocol Alpha�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(h��Protocol Alpha, whose Alpha has nothing to do with the one in Alphanet,
is the name of the initial economic protocol. Alpha is a placeholder
name, while we decide on the naming convention for protocol versions.�h]�h.��Protocol Alpha, whose Alpha has nothing to do with the one in Alphanet,
is the name of the initial economic protocol. Alpha is a placeholder
name, while we decide on the naming convention for protocol versions.�����}�(hh=hh;hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(h�.Before reading that document, you may want to:�h]�h.�.Before reading that document, you may want to:�����}�(hhKhhIhhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK
hh$hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�read the whitepaper,�h]�h:)��}�(hh`h]�h.�read the whitepaper,�����}�(hh`hhbubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh^ubah}�(h]�h]�h]�h]�h]�uhh\hhYhhh h!hNubh])��}�(h�Kread :ref:`how the economic protocol is
sandboxed <protocol_environment>`.
�h]�h:)��}�(h�Jread :ref:`how the economic protocol is
sandboxed <protocol_environment>`.�h]�(h.�read �����}�(h�read �hhyub�sphinx.addnodes��pending_xref���)��}�(h�D:ref:`how the economic protocol is
sandboxed <protocol_environment>`�h]�h �inline���)��}�(hh�h]�h.�&how the economic protocol is
sandboxed�����}�(hhhh�ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit���	reftarget��protocol_environment��refdoc��tutorials/entering_alpha��refwarn��uhh�h h!hKhhyubh.�.�����}�(h�.�hhyubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhhuubah}�(h]�h]�h]�h]�h]�uhh\hhYhhh h!hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhhWh h!hKhh$hhubh:)��}�(h��As all protocols, Alpha is made of a series of OCaml interface and
implementation files, accompanied by a ``TEZOS_PROTOCOL`` file.�h]�(h.�jAs all protocols, Alpha is made of a series of OCaml interface and
implementation files, accompanied by a �����}�(h�jAs all protocols, Alpha is made of a series of OCaml interface and
implementation files, accompanied by a �hh�hhh NhNubh �literal���)��}�(h�``TEZOS_PROTOCOL``�h]�h.�TEZOS_PROTOCOL�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh.� file.�����}�(h� file.�hh�hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh#)��}�(hhh]�(h()��}�(h� The ``TEZOS_PROTOCOL`` structure�h]�(h.�The �����}�(h�The �hh�hhh NhNubh�)��}�(h�``TEZOS_PROTOCOL``�h]�h.�TEZOS_PROTOCOL�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh.�
 structure�����}�(h�
 structure�hh�hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hKubh:)��}�(h��If you look at this file in the repository, you will see that it is
composed of the hash of the sources, and the list of its modules, in
linking order.�h]�h.��If you look at this file in the repository, you will see that it is
composed of the hash of the sources, and the list of its modules, in
linking order.�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�hhubh:)��}�(hX  Protocol Alpha is structured as a tower of abstraction layers, a coding
discipline that we designed to have OCaml check as many invariants as
possible at typing time. You will also see empty lines in
``TEZOS_PROTOCOL`` that denote these layers of abstraction.�h]�(h.��Protocol Alpha is structured as a tower of abstraction layers, a coding
discipline that we designed to have OCaml check as many invariants as
possible at typing time. You will also see empty lines in
�����}�(h��Protocol Alpha is structured as a tower of abstraction layers, a coding
discipline that we designed to have OCaml check as many invariants as
possible at typing time. You will also see empty lines in
�hj  hhh NhNubh�)��}�(h�``TEZOS_PROTOCOL``�h]�h.�TEZOS_PROTOCOL�����}�(hhhj#  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh.�) that denote these layers of abstraction.�����}�(h�) that denote these layers of abstraction.�hj  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�hhubh:)��}�(h��These layers follow the linking order: the first modules are the tower’s
foundation that talk to the raw key-value store, and going forward in
the module list means climbing up the abstraction tower.�h]�h.��These layers follow the linking order: the first modules are the tower’s
foundation that talk to the raw key-value store, and going forward in
the module list means climbing up the abstraction tower.�����}�(hj>  hj<  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�hhubeh}�(h]��the-tezos-protocol-structure�ah]�h]��the tezos_protocol structure�ah]�h]�uhh"hh$hhh h!hKubh#)��}�(hhh]�(h()��}�(h�.The big abstraction barrier: ``Alpha_context``�h]�(h.�The big abstraction barrier: �����}�(h�The big abstraction barrier: �hjU  hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhj^  ubah}�(h]�h]�h]�h]�h]�uhh�hjU  ubeh}�(h]�h]�h]�h]�h]�uhh'hjR  hhh h!hK$ubh:)��}�(h��The proof-of-stake algorithm, as described in the white paper, relies on
an abstract state of the ledger, that is read and transformed during
validation of a block.�h]�h.��The proof-of-stake algorithm, as described in the white paper, relies on
an abstract state of the ledger, that is read and transformed during
validation of a block.�����}�(hjt  hjr  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK&hjR  hhubh:)��}�(hX&  Due to the polymorphic nature of Tezos, the ledger’s state (that we call
**context** in the code), cannot be specific to protocol Alpha’s need.
The proof-of-stake is thus implemented over a generic key-value store
whose keys and associated binary data must implement the abstract
structure.�h]�(h.�KDue to the polymorphic nature of Tezos, the ledger’s state (that we call
�����}�(h�KDue to the polymorphic nature of Tezos, the ledger’s state (that we call
�hj�  hhh NhNubh �strong���)��}�(h�**context**�h]�h.�context�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh.�� in the code), cannot be specific to protocol Alpha’s need.
The proof-of-stake is thus implemented over a generic key-value store
whose keys and associated binary data must implement the abstract
structure.�����}�(h�� in the code), cannot be specific to protocol Alpha’s need.
The proof-of-stake is thus implemented over a generic key-value store
whose keys and associated binary data must implement the abstract
structure.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK*hjR  hhubh:)��}�(hX  The ``Alpha_context`` module enforces the separation of concerns
between, on one hand, mapping the abstract state of the ledger to the
concrete structure of the key-value store, and, on the other hand,
implementing the proof-of-stake algorithm over this state.�h]�(h.�The �����}�(h�The �hj�  hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�� module enforces the separation of concerns
between, on one hand, mapping the abstract state of the ledger to the
concrete structure of the key-value store, and, on the other hand,
implementing the proof-of-stake algorithm over this state.�����}�(h�� module enforces the separation of concerns
between, on one hand, mapping the abstract state of the ledger to the
concrete structure of the key-value store, and, on the other hand,
implementing the proof-of-stake algorithm over this state.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK0hjR  hhubh:)��}�(hXr  In more practical terms, ``Alpha_context`` defines a type ``t`` that
represents a state of the ledger. This state is an abstracted out
version of the key-value store that can only be manipulated through the
use of the few selected manipulations reexported by ``Alpha_context``,
that always preserve the well-typed aspect and internal consistency
invariants of the state.�h]�(h.�In more practical terms, �����}�(h�In more practical terms, �hj�  hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.� defines a type �����}�(h� defines a type �hj�  hhh NhNubh�)��}�(h�``t``�h]�h.�t�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�� that
represents a state of the ledger. This state is an abstracted out
version of the key-value store that can only be manipulated through the
use of the few selected manipulations reexported by �����}�(h�� that
represents a state of the ledger. This state is an abstracted out
version of the key-value store that can only be manipulated through the
use of the few selected manipulations reexported by �hj�  hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�^,
that always preserve the well-typed aspect and internal consistency
invariants of the state.�����}�(h�^,
that always preserve the well-typed aspect and internal consistency
invariants of the state.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK5hjR  hhubh:)��}�(hX.  When validating a block, the low-level state that result from the
predecessor block is read from the disk, then abstracted out to a
``Alpha_context.t``, which is then only updated by high level operations
that preserve consistency, and finally, the low level state is extracted
to be committed on disk.�h]�(h.��When validating a block, the low-level state that result from the
predecessor block is read from the disk, then abstracted out to a
�����}�(h��When validating a block, the low-level state that result from the
predecessor block is read from the disk, then abstracted out to a
�hj  hhh NhNubh�)��}�(h�``Alpha_context.t``�h]�h.�Alpha_context.t�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh.��, which is then only updated by high level operations
that preserve consistency, and finally, the low level state is extracted
to be committed on disk.�����}�(h��, which is then only updated by high level operations
that preserve consistency, and finally, the low level state is extracted
to be committed on disk.�hj  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK<hjR  hhubh:)��}�(hX+  This way, we have two well separated parts in the code. The code below
``Alpha_context`` implements the ledger’s state storage, while the code
on top of it is the proof-of-stake algorithm. Thanks to this barrier,
the latter can remain nice, readable OCaml that only manipulates plain
OCaml values.�h]�(h.�GThis way, we have two well separated parts in the code. The code below
�����}�(h�GThis way, we have two well separated parts in the code. The code below
�hj0  hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhj9  ubah}�(h]�h]�h]�h]�h]�uhh�hj0  ubh.�� implements the ledger’s state storage, while the code
on top of it is the proof-of-stake algorithm. Thanks to this barrier,
the latter can remain nice, readable OCaml that only manipulates plain
OCaml values.�����}�(h�� implements the ledger’s state storage, while the code
on top of it is the proof-of-stake algorithm. Thanks to this barrier,
the latter can remain nice, readable OCaml that only manipulates plain
OCaml values.�hj0  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKBhjR  hhubeh}�(h]��)the-big-abstraction-barrier-alpha-context�ah]�h]��*the big abstraction barrier: alpha_context�ah]�h]�uhh"hh$hhh h!hK$ubh#)��}�(hhh]�(h()��}�(h�Below the ``Alpha_context``�h]�(h.�
Below the �����}�(h�
Below the �hj]  hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhjf  ubah}�(h]�h]�h]�h]�h]�uhh�hj]  ubeh}�(h]�h]�h]�h]�h]�uhh'hjZ  hhh h!hKIubh:)��}�(h��For this part, in a first discovery of the source code, you can start by
relying mostly on this coarse grained description, with a little bit of
cherry-picking when you’re curious about how a specific invariant is
enforced.�h]�h.��For this part, in a first discovery of the source code, you can start by
relying mostly on this coarse grained description, with a little bit of
cherry-picking when you’re curious about how a specific invariant is
enforced.�����}�(hj|  hjz  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKKhjZ  hhubh#)��}�(hhh]�(h()��}�(h�The ``*_repr`` modules�h]�(h.�The �����}�(h�The �hj�  hhh NhNubh�)��}�(h�
``*_repr``�h]�h.�*_repr�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.� modules�����}�(h� modules�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh'hj�  hhh h!hKQubh:)��}�(h�lThese modules abstract the values of the raw key-value context by using
:ref:`Data_encoding<data_encoding>`.�h]�(h.�HThese modules abstract the values of the raw key-value context by using
�����}�(h�HThese modules abstract the values of the raw key-value context by using
�hj�  hhh NhNubh�)��}�(h�#:ref:`Data_encoding<data_encoding>`�h]�h�)��}�(hj�  h]�h.�Data_encoding�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h��data_encoding�h�h�h��uhh�h h!hKShj�  ubh.�.�����}�(hh�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKShj�  hhubh:)��}�(h��These modules define the data types used by the protocol that need to be
serialized (amounts, contract handles, script expressions, etc.). For
each type, it also defines its serialization format using
:ref:`Data_encoding<data_encoding>`.�h]�(h.��These modules define the data types used by the protocol that need to be
serialized (amounts, contract handles, script expressions, etc.). For
each type, it also defines its serialization format using
�����}�(h��These modules define the data types used by the protocol that need to be
serialized (amounts, contract handles, script expressions, etc.). For
each type, it also defines its serialization format using
�hj�  hhh NhNubh�)��}�(h�#:ref:`Data_encoding<data_encoding>`�h]�h�)��}�(hj�  h]�h.�Data_encoding�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h��data_encoding�h�h�h��uhh�h h!hKVhj�  ubh.�.�����}�(hh�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKVhj�  hhubh:)��}�(h��Above this layer, the code should never see the byte sequences in the
database, the ones of transmitted blocks and operations, or the raw JSON
of data transmitted via RPCs. It only manipulates OCaml values.�h]�h.��Above this layer, the code should never see the byte sequences in the
database, the ones of transmitted blocks and operations, or the raw JSON
of data transmitted via RPCs. It only manipulates OCaml values.�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK[hj�  hhubeh}�(h]��the-repr-modules�ah]�h]��the *_repr modules�ah]�h]�uhh"hjZ  hhh h!hKQubh#)��}�(hhh]�(h()��}�(h�+The ``Storage`` module and storage functors�h]�(h.�The �����}�(h�The �hj(  hhh NhNubh�)��}�(h�``Storage``�h]�h.�Storage�����}�(hhhj1  ubah}�(h]�h]�h]�h]�h]�uhh�hj(  ubh.� module and storage functors�����}�(h� module and storage functors�hj(  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh'hj%  hhh h!hK`ubh:)��}�(h��Even with the concrete formats of values in the context abstracted out,
type (or consistency) errors can still occur if the code accesses a
value with a wrong key, or a key bound to another value. The next
abstraction barrier is a remedy to that.�h]�h.��Even with the concrete formats of values in the context abstracted out,
type (or consistency) errors can still occur if the code accesses a
value with a wrong key, or a key bound to another value. The next
abstraction barrier is a remedy to that.�����}�(hjL  hjJ  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKbhj%  hhubh:)��}�(h��The storage module is the single place in the protocol where key
literals are defined. Hence, it is the only module necessary to audit,
to know that the keys are not colliding.�h]�h.��The storage module is the single place in the protocol where key
literals are defined. Hence, it is the only module necessary to audit,
to know that the keys are not colliding.�����}�(hjZ  hjX  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKghj%  hhubh:)��}�(h��It also abstracts the keys, so that each kind of key get its own
accessors. For instance, module ``Storage.Contract.Balance`` contains
accessors specific to contracts’ balances.�h]�(h.�aIt also abstracts the keys, so that each kind of key get its own
accessors. For instance, module �����}�(h�aIt also abstracts the keys, so that each kind of key get its own
accessors. For instance, module �hjf  hhh NhNubh�)��}�(h�``Storage.Contract.Balance``�h]�h.�Storage.Contract.Balance�����}�(hhhjo  ubah}�(h]�h]�h]�h]�h]�uhh�hjf  ubh.�6 contains
accessors specific to contracts’ balances.�����}�(h�6 contains
accessors specific to contracts’ balances.�hjf  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKkhj%  hhubh:)��}�(hX&  Moreover, the keys bear the type of the values they point to. For
instance, only values of type ``Tez_repr.t`` can by stored at keys
``Storage.Contract.Balance``. And in case a key is not a global key, but
a parametric one, this key is parameterized by an OCaml value, and not the
raw key part.�h]�(h.�`Moreover, the keys bear the type of the values they point to. For
instance, only values of type �����}�(h�`Moreover, the keys bear the type of the values they point to. For
instance, only values of type �hj�  hhh NhNubh�)��}�(h�``Tez_repr.t``�h]�h.�
Tez_repr.t�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.� can by stored at keys
�����}�(h� can by stored at keys
�hj�  hhh NhNubh�)��}�(h�``Storage.Contract.Balance``�h]�h.�Storage.Contract.Balance�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.��. And in case a key is not a global key, but
a parametric one, this key is parameterized by an OCaml value, and not the
raw key part.�����}�(h��. And in case a key is not a global key, but
a parametric one, this key is parameterized by an OCaml value, and not the
raw key part.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKohj%  hhubh:)��}�(h��So in the end, the only way to be used when accessing a contract balance
is ``Storage.Contract.Balance.get``, which takes a ``Contract_repr.t``
and gives a ``Tez_repr.t``.�h]�(h.�LSo in the end, the only way to be used when accessing a contract balance
is �����}�(h�LSo in the end, the only way to be used when accessing a contract balance
is �hj�  hhh NhNubh�)��}�(h� ``Storage.Contract.Balance.get``�h]�h.�Storage.Contract.Balance.get�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�, which takes a �����}�(h�, which takes a �hj�  hhh NhNubh�)��}�(h�``Contract_repr.t``�h]�h.�Contract_repr.t�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�
and gives a �����}�(h�
and gives a �hj�  hhh NhNubh�)��}�(h�``Tez_repr.t``�h]�h.�
Tez_repr.t�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�.�����}�(hh�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKuhj%  hhubh:)��}�(h�{All these well-typed operations are generated by a set of functors, that
come just before ``Storage`` in ``TEZOS_CONTEXT``.�h]�(h.�ZAll these well-typed operations are generated by a set of functors, that
come just before �����}�(h�ZAll these well-typed operations are generated by a set of functors, that
come just before �hj  hhh NhNubh�)��}�(h�``Storage``�h]�h.�Storage�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh.� in �����}�(h� in �hj  hhh NhNubh�)��}�(h�``TEZOS_CONTEXT``�h]�h.�TEZOS_CONTEXT�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh.�.�����}�(hh�hj  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKyhj%  hhubeh}�(h]��'the-storage-module-and-storage-functors�ah]�h]��'the storage module and storage functors�ah]�h]�uhh"hjZ  hhh h!hK`ubh#)��}�(hhh]�(h()��}�(h�The ``*_storage`` modules�h]�(h.�The �����}�(h�The �hjC  hhh NhNubh�)��}�(h�``*_storage``�h]�h.�	*_storage�����}�(hhhjL  ubah}�(h]�h]�h]�h]�h]�uhh�hjC  ubh.� modules�����}�(h� modules�hjC  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh'hj@  hhh h!hK}ubh:)��}�(h�kThe two previous steps ensure that the ledger’s state is always accessed
and updated in a well-typed way.�h]�h.�kThe two previous steps ensure that the ledger’s state is always accessed
and updated in a well-typed way.�����}�(hjg  hje  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhj@  hhubh:)��}�(h��However, it does not enforce that, for instance, when a contract is
deleted, all of the keys that store its state in the context are indeed
deleted.�h]�h.��However, it does not enforce that, for instance, when a contract is
deleted, all of the keys that store its state in the context are indeed
deleted.�����}�(hju  hjs  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj@  hhubh:)��}�(h��This last series of modules named ``*_storage`` is there to enforce just
that kind of invariants: ensuring the internal consistency of the
context structure.�h]�(h.�"This last series of modules named �����}�(h�"This last series of modules named �hj�  hhh NhNubh�)��}�(h�``*_storage``�h]�h.�	*_storage�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�n is there to enforce just
that kind of invariants: ensuring the internal consistency of the
context structure.�����}�(h�n is there to enforce just
that kind of invariants: ensuring the internal consistency of the
context structure.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj@  hhubh:)��}�(h��These transaction do not go as far as checking that, for instance, when
the destination of a transaction is credited, the source is also
debited, as in some cases, it might not be the case.�h]�h.��These transaction do not go as far as checking that, for instance, when
the destination of a transaction is credited, the source is also
debited, as in some cases, it might not be the case.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj@  hhubeh}�(h]��the-storage-modules�ah]�h]��the *_storage modules�ah]�h]�uhh"hjZ  hhh h!hK}ubeh}�(h]��below-the-alpha-context�ah]�h]��below the alpha_context�ah]�h]�uhh"hh$hhh h!hKIubh#)��}�(hhh]�(h()��}�(h�Above the ``Alpha_context``�h]�(h.�
Above the �����}�(h�
Above the �hj�  hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh'hj�  hhh h!hK�ubh:)��}�(h��The three next sections describe the main entrypoints to the protocol:
validation of blocks by the shell (that we often also call application),
smart contracts, and RPC services.�h]�h.��The three next sections describe the main entrypoints to the protocol:
validation of blocks by the shell (that we often also call application),
smart contracts, and RPC services.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj�  hhubh:)��}�(h��The ``Main`` module is the entrypoint that’s used by the shell. It
respects the module type that all protocol must follow. For that, its
code is mostly plumbing,�h]�(h.�The �����}�(h�The �hj�  hhh NhNubh�)��}�(h�``Main``�h]�h.�Main�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�� module is the entrypoint that’s used by the shell. It
respects the module type that all protocol must follow. For that, its
code is mostly plumbing,�����}�(h�� module is the entrypoint that’s used by the shell. It
respects the module type that all protocol must follow. For that, its
code is mostly plumbing,�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj�  hhubh#)��}�(hhh]�(h()��}�(h�Starting from ``Apply``�h]�(h.�Starting from �����}�(h�Starting from �hj  hhh NhNubh�)��}�(h�	``Apply``�h]�h.�Apply�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubeh}�(h]�h]�h]�h]�h]�uhh'hj  hhh h!hK�ubh:)��}�(hX   This is were you want to start on your first read. Even if some plumbing
code is woven in, such as error cases declaration and registration, most
of the proof-of-stake code has been written in a verbose style, to be
understood with minimum OCaml knowledge.�h]�h.X   This is were you want to start on your first read. Even if some plumbing
code is woven in, such as error cases declaration and registration, most
of the proof-of-stake code has been written in a verbose style, to be
understood with minimum OCaml knowledge.�����}�(hj3  hj1  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj  hhubh:)��}�(hX(  You want to start from the shell entry points (validation of the block
header, validation of an operation, finalization of a block validation),
and follow the control flow until you hit the ``Alpha_context``
abstraction barrier. This will lead you to reading modules ``Baking``
and ``Amendment``.�h]�(h.��You want to start from the shell entry points (validation of the block
header, validation of an operation, finalization of a block validation),
and follow the control flow until you hit the �����}�(h��You want to start from the shell entry points (validation of the block
header, validation of an operation, finalization of a block validation),
and follow the control flow until you hit the �hj?  hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhjH  ubah}�(h]�h]�h]�h]�h]�uhh�hj?  ubh.�<
abstraction barrier. This will lead you to reading modules �����}�(h�<
abstraction barrier. This will lead you to reading modules �hj?  hhh NhNubh�)��}�(h�
``Baking``�h]�h.�Baking�����}�(hhhj[  ubah}�(h]�h]�h]�h]�h]�uhh�hj?  ubh.�
and �����}�(h�
and �hj?  hhh NhNubh�)��}�(h�``Amendment``�h]�h.�	Amendment�����}�(hhhjn  ubah}�(h]�h]�h]�h]�h]�uhh�hj?  ubh.�.�����}�(hh�hj?  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj  hhubeh}�(h]��starting-from-apply�ah]�h]��starting from apply�ah]�h]�uhh"hj�  hhh h!hK�ubh#)��}�(hhh]�(h()��}�(h�Smart contracts�h]�h.�Smart contracts�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj�  hhh h!hK�ubh:)��}�(hX%  From ``Apply``, you will also end up in modules ``Script_ir_translator``
and ``Script_interpreter``. The former is the typechecker of Michelson
that is called when creating a new smart contract, and the latter is the
interpreter that is called when transferring tokens to a new smart
contract.�h]�(h.�From �����}�(h�From �hj�  hhh NhNubh�)��}�(h�	``Apply``�h]�h.�Apply�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�", you will also end up in modules �����}�(h�", you will also end up in modules �hj�  hhh NhNubh�)��}�(h�``Script_ir_translator``�h]�h.�Script_ir_translator�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�
and �����}�(h�
and �hj�  hhh NhNubh�)��}�(h�``Script_interpreter``�h]�h.�Script_interpreter�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.��. The former is the typechecker of Michelson
that is called when creating a new smart contract, and the latter is the
interpreter that is called when transferring tokens to a new smart
contract.�����}�(h��. The former is the typechecker of Michelson
that is called when creating a new smart contract, and the latter is the
interpreter that is called when transferring tokens to a new smart
contract.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj�  hhubeh}�(h]��smart-contracts�ah]�h]��smart contracts�ah]�h]�uhh"hj�  hhh h!hK�ubh#)��}�(hhh]�(h()��}�(h�Protocol RPC API�h]�h.�Protocol RPC API�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj�  hhh h!hK�ubh:)��}�(h�YFinally, the RPCs specific to Alpha are also defined above the
``Alpha_context`` barrier.�h]�(h.�?Finally, the RPCs specific to Alpha are also defined above the
�����}�(h�?Finally, the RPCs specific to Alpha are also defined above the
�hj   hhh NhNubh�)��}�(h�``Alpha_context``�h]�h.�Alpha_context�����}�(hhhj	  ubah}�(h]�h]�h]�h]�h]�uhh�hj   ubh.�	 barrier.�����}�(h�	 barrier.�hj   hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj�  hhubh:)��}�(hX�  Services are defined in a few modules, divided by theme. Each module
defines the RPC API: URL schemes with the types of parameters, and
input and output JSON schemas. This interface serves three
purposes. As it is thoroughly typed, it makes sure that the handlers
(that are registered in the same file) have the right input and output
types. It is also used by the client to perform RPC calls, to make
sure that the URL schemes and JSON formats and consistent between the
two parties. These two features are extremely useful when refactoring,
as the OCaml typechecker will help us track the effects of an RPC API
change on the whole codebase. The third purpose is of course, to make
automatic documentation generation possible (as in ``tezos client rpc
list/format``). Each service is also accompanied by a caller function,
that can be used from the client to perform the calls, and by the
tests to simulate calls in a fake in-memory context.�h]�(h.X�  Services are defined in a few modules, divided by theme. Each module
defines the RPC API: URL schemes with the types of parameters, and
input and output JSON schemas. This interface serves three
purposes. As it is thoroughly typed, it makes sure that the handlers
(that are registered in the same file) have the right input and output
types. It is also used by the client to perform RPC calls, to make
sure that the URL schemes and JSON formats and consistent between the
two parties. These two features are extremely useful when refactoring,
as the OCaml typechecker will help us track the effects of an RPC API
change on the whole codebase. The third purpose is of course, to make
automatic documentation generation possible (as in �����}�(hX�  Services are defined in a few modules, divided by theme. Each module
defines the RPC API: URL schemes with the types of parameters, and
input and output JSON schemas. This interface serves three
purposes. As it is thoroughly typed, it makes sure that the handlers
(that are registered in the same file) have the right input and output
types. It is also used by the client to perform RPC calls, to make
sure that the URL schemes and JSON formats and consistent between the
two parties. These two features are extremely useful when refactoring,
as the OCaml typechecker will help us track the effects of an RPC API
change on the whole codebase. The third purpose is of course, to make
automatic documentation generation possible (as in �hj"  hhh NhNubh�)��}�(h� ``tezos client rpc
list/format``�h]�h.�tezos client rpc
list/format�����}�(hhhj+  ubah}�(h]�h]�h]�h]�h]�uhh�hj"  ubh.��). Each service is also accompanied by a caller function,
that can be used from the client to perform the calls, and by the
tests to simulate calls in a fake in-memory context.�����}�(h��). Each service is also accompanied by a caller function,
that can be used from the client to perform the calls, and by the
tests to simulate calls in a fake in-memory context.�hj"  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj�  hhubh:)��}�(h��It can be useful if you are a third party developer who wants to read
the OCaml definition of the service hierarchy directly, instead of the
automatically generated JSON hierarchy.�h]�h.��It can be useful if you are a third party developer who wants to read
the OCaml definition of the service hierarchy directly, instead of the
automatically generated JSON hierarchy.�����}�(hjF  hjD  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK�hj�  hhubeh}�(h]��protocol-rpc-api�ah]�h]��protocol rpc api�ah]�h]�uhh"hj�  hhh h!hK�ubeh}�(h]��above-the-alpha-context�ah]�h]��above the alpha_context�ah]�h]�uhh"hh$hhh h!hK�ubeh}�(h]�(�#how-to-start-reading-protocol-alpha�heh]�h]�(�#how to start reading protocol alpha��entering_alpha�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�jh  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(jh  hjg  jd  jO  jL  jW  jT  j�  j�  j"  j  j=  j:  j�  j�  j_  j\  j�  j�  j�  j�  jW  jT  u�	nametypes�}�(jh  �jg  NjO  NjW  Nj�  Nj"  Nj=  Nj�  Nj_  Nj�  Nj�  NjW  Nuh}�(hh$jd  h$jL  h�jT  jR  j�  jZ  j  j�  j:  j%  j�  j@  j\  j�  j�  j  j�  j�  jT  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h:)��}�(hhh]�h.�4Hyperlink target "entering-alpha" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj�  uba�transformer�N�
decoration�Nhhub.